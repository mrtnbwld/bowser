// returns all available locale keys
let translations = {};
const context = require.context('./locales', false, /\.(json)$/);

context.keys().forEach( filename => {
  let key = filename.replace('./', '').replace('.json', '');
  translations[key] = context(filename);
});

export default translations;
