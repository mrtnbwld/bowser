import React from 'react'
import ReactDOM from 'react-dom'
import Root from './components/Root'
import {Router, Route, IndexRoute} from 'react-router'
import {history, store} from './store/createStore'
import {Provider} from 'react-redux'

import {loadTranslations, syncTranslationWithStore, setLocale} from 'react-redux-i18n';
import {localePreferences} from './utils/LocaleDetection'
import checkTrackerStatus from './utils/checkTrackerStatus'
import translations from './translations'
import AppContainer from './containers/AppContainer'
import SupportContainer from './containers/SupportContainer'
import HomeContainer from './containers/HomeContainer'
import DevicesContainer from './containers/DevicesContainer'
import ErrorContainer from './containers/ErrorContainer'
import MessageSentContainer from './containers/MessageSentContainer'
import PrivacyContainer from './containers/PrivacyContainer'
import UserIdentificationContainer from './containers/UserIdentificationContainer'


let lang = localePreferences()
syncTranslationWithStore(store)
store.dispatch(setLocale(lang));
store.dispatch(loadTranslations(translations));


// TODO: create initialisation function that pre-populates the store with tracker state data so that we can always read from the store.
ReactDOM.render(
  <Provider store={store}>
    <Root>
      <Router history={history}>
        <Route path="/" component={AppContainer} onEnter={checkTrackerStatus}>
          <IndexRoute component={HomeContainer}/>
          <Route path="/my-devices" component={DevicesContainer}/>
          <Route path="/privacy" component={PrivacyContainer}/>
          <Route path="/support" component={SupportContainer}/>
          <Route path="/message-sent" component={MessageSentContainer}/>
        </Route>
        <Route path="/error" component={ErrorContainer}/>
        <Route path="/user-identification" component={UserIdentificationContainer} onEnter={checkTrackerStatus}/>
      </Router>
    </Root>
  </Provider>
  ,
  document.getElementById('root')
)

