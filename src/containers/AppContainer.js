import App from '../components/App'
import {connect} from 'react-redux'
import {getTrackerState} from '../actions/trackerStates'
import {connection} from '../utils/CoreConnection'
import {getParticipationStatus, deviceAuthHash} from '../actions/participationStatus'
import {APP_START_FINISHED} from '../actions'

const mapStateToProps = state => {return {
  appStarted: state.tracker.appStarted
}}

const mapDispatchToProps = dispatch => {return {
  getTrackerState: () => {
    dispatch(getTrackerState())
  },
  getParticipationStatus: () => {
    connection.getDeviceToken( authHash => {
      dispatch(deviceAuthHash(authHash))
      dispatch(getParticipationStatus(authHash))
      dispatch({type: APP_START_FINISHED})
    })
  },
}}

export default connect(mapStateToProps, mapDispatchToProps)(App)
