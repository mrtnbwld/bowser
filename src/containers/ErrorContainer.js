import DisplayError from '../components/DisplayError'
import {connect} from 'react-redux'

const mapStateToProps = state => {return {
  errors: state.errors
}}

const mapDispatchToProps = dispatch => {return {}}

export default connect(mapStateToProps, mapDispatchToProps)(DisplayError)
