import AddDevice from '../components/AddDevice'
import {connect} from 'react-redux'

const mapStateToProps = state => {return {}}

const mapDispatchToProps = dispatch => {return {}}

export default connect(mapStateToProps, mapDispatchToProps)(AddDevice)
