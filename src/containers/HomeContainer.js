import Home from '../components/Home'
import {connect} from 'react-redux'
import {pauseTracker, unpauseTracker, getTrackerState} from '../actions/trackerStates'

const mapStateToProps = state => {return {
  tracker: state.tracker,
  participation: state.participation,
  errors: state.errors
}}

const mapDispatchToProps = dispatch => {return {
  pauseTracker: (duration) => {
    dispatch(pauseTracker(duration))
  },
  unpauseTracker: (duration) => {
    dispatch(unpauseTracker())
  },
  getTrackerState: () => {
    dispatch(getTrackerState())
  }
}}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
