import {SupportPage} from '../components/Support'
import {connect} from 'react-redux'
import {submitForm} from '../actions/supportForm'

const mapStateToProps = state => {return {
  warnings: state.warnings
}}

const mapDispatchToProps = dispatch => {return {
  submitForm: (data, email) => dispatch(submitForm(data,email))
}}

export default connect(mapStateToProps, mapDispatchToProps)(SupportPage)
