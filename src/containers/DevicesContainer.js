import Devices from '../components/Devices'
import {connect} from 'react-redux'
import {getDevicesList} from '../actions/getDevicesList'

const mapStateToProps = state => {return {
  devices: state.devices,
  warnings: state.warnings
}}

const mapDispatchToProps = dispatch => {return {
  getDevicesList: (id) => {
    dispatch(getDevicesList(id))
  }
}}

export default connect(mapStateToProps, mapDispatchToProps)(Devices)
