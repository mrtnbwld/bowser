import React from 'react'
import PropTypes from 'prop-types'
import styles from './KillSwitch.sass'

const KillSwitch = ({title, text, action}) => {
  return (
    <div data-test-id='killswitch-page' className={styles['killswitch-container']}>
      <div className={styles['killswitch-card']}>
        <div className={styles['killswitch-header']}>
          <img src={require("../../assets/images/bell-icon.png")} alt="bell-icon"/>
          <h1 className={styles['killswitch-title']}>{title}</h1>
        </div>
        {text}
        {action ? action : undefined}
      </div>
    </div>
  )
}

KillSwitch.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.node.isRequired,
  action: PropTypes.func
}


export default KillSwitch
