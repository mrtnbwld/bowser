import React, { Component } from 'react'
import styles from './DisplayError.sass'
import Navigation from '../Navigation'
import WarningCard from '../WarningCard'

class DisplayError extends Component {

  render() {
    return (
      <div data-test-id='error-page' className={styles['error-page']}>
        <Navigation location={this.props.location} displayMenuButton={false}/>
        <WarningCard error={{
          priority: 'required',
          name: 'notConfigured'
        }}/>
      </div>
    )
  }
}

export default DisplayError;
