import React, { Component } from 'react';
import styles from './Pause.sass'

class Pause extends Component {

  render() {
    return (
      <div data-test-id={this.props.testID || 'pause-icon'} className={styles['pause-icon']}>
        <svg width="44px" height="44px" viewBox="0 0 44 44">
            <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Extensions/Open" transform="translate(-166.000000, -523.000000)">
                    <g id="Open">
                        <g id="Actions" transform="translate(163.000000, 523.000000)">
                            <g id="Pause">
                                <path d="M3,3.0069809 C3,1.34627121 4.33396149,0 6.00012623,0 L16.9998738,0 C18.6567977,0 20,1.34085738 20,3.0069809 L20,40.9930191 C20,42.6537288 18.6660385,44 16.9998738,44 L6.00012623,44 C4.34320226,44 3,42.6591426 3,40.9930191 L3,3.0069809 Z M30,3.0069809 C30,1.34627121 31.3339615,0 33.0001262,0 L43.9998738,0 C45.6567977,0 47,1.34085738 47,3.0069809 L47,40.9930191 C47,42.6537288 45.6660385,44 43.9998738,44 L33.0001262,44 C31.3432023,44 30,42.6591426 30,40.9930191 L30,3.0069809 Z" id="Pause-icon"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
      </div>
    )
  }
}

export default Pause;
