import React, { Component } from 'react';
import styles from './Add.sass'
import clientConfig from '../../../clientConfig'

class Add extends Component {

  render() {
    return (
      <div data-test-id={this.props.testID || 'add-icon'} className={styles['add-icon']}>
        <svg width="45px" height="44px" viewBox="0 0 45 44" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
            <g id="The-app---Status,-my-devices,-menu,-warnings" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="1b-Paused" transform="translate(-249.000000, -538.000000)" fill="#007FFF">
                    <g id="Actions" transform="translate(52.000000, 538.000000)">
                        <g id="Add-device" transform="translate(173.000000, 0.000000)">
                            <path d="M55,30 L65.9950833,30 C67.6532394,30 69,28.652611 69,26.9905224 L69,17.0094776 C69,15.3366311 67.654653,14 65.9950833,14 L55,14 L55,3.0069809 C55,1.34085738 53.6567977,0 51.9998738,0 L41.0001262,0 C39.3339615,0 38,1.34627121 38,3.0069809 L38,14 L27.0049167,14 C25.3467606,14 24,15.347389 24,17.0094776 L24,26.9905224 C24,28.6633689 25.345347,30 27.0049167,30 L38,30 L38,40.9930191 C38,42.6591426 39.3432023,44 41.0001262,44 L51.9998738,44 C53.6660385,44 55,42.6537288 55,40.9930191 L55,30 Z" id="Add-icon"></path>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
      </div>
    )
  }
}

export default Add;
