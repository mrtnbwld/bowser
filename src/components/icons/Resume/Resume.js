import React, { Component } from 'react';
import styles from './Resume.sass'

class Resume extends Component {

  render() {
    return (
      <div data-test-id={this.props.testID || 'resume-icon'} className={styles['resume-icon']}>
        <svg width="35px" height="43px" viewBox="0 0 35 43">
            <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Extensions/Paused" transform="translate(-170.000000, -524.000000)">
                    <g id="Open">
                        <g id="Actions" transform="translate(155.000000, 524.000000)">
                            <g id="Resume">
                                <path d="M47.404242,25.1079226 L21.6525265,42.275733 C19.7211478,43.5633188 16.9313787,43.1341235 15.6437929,40.9881472 C15.2145976,40.3443543 15,39.4859638 15,38.6275733 L15,4.29195259 C15,1.93137867 16.9313787,0 19.2919526,0 C20.1503431,0 21.0087336,0.214597629 21.6525265,0.643792888 L47.404242,17.8116032 C49.3356207,19.099189 49.9794136,21.8889582 48.4772302,23.8203369 C48.2626326,24.2495321 47.8334373,24.6787274 47.404242,25.1079226 Z" id="Resume-icon"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
      </div>
    )
  }
}

export default Resume;
