import React, { Component } from 'react'
import styles from './Tablet.sass'
import Checkmark from '../Checkmark'
import Lock from '../Lock'
import Stop from '../Stop'

class Tablet extends Component {

  render() {
    let size = this.props.size || 'large',
        color

    this.props.clientColor && (color = 'clientcolor')

    let renderDeviceStatus = () => {
      switch(this.props.status){
        case 'inactive': return <Stop/>
        case 'paused': return <Lock/>
        case 'active':
        default:
          return <Checkmark/>
      }
    }

    return (
      <div data-test-id={this.props.testID || 'tablet-icon'} className={styles['tablet-icon-' + size] + ' ' + styles[color]}>
        <svg width="46px" height="63px" viewBox="34 13 46 63">
            <g id="Device-icon---Active" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(34.000000, 13.000000)">
                <g id="Tablet">
                    <path d="M2,4.00071403 L2,58.999286 C2,60.1082894 2.89304612,61 4.00172845,61 L41.9982716,61 C43.1053515,61 44,60.1051925 44,58.999286 L44,4.00071403 C44,2.8917106 43.1069539,2 41.9982716,2 L4.00172845,2 C2.89464845,2 2,2.89480755 2,4.00071403 Z M0,4.00071403 C0,1.79118068 1.78913646,0 4.00172845,0 L41.9982716,0 C44.2083651,0 46,1.78398548 46,4.00071403 L46,58.999286 C46,61.2088193 44.2108635,63 41.9982716,63 L4.00172845,63 C1.79163485,63 0,61.2160145 0,58.999286 L0,4.00071403 Z" id="Rectangle-6"></path>
                    <ellipse id="Oval" cx="23.2" cy="5.21656051" rx="0.8" ry="0.802547771"></ellipse>
                    <path d="M23,56.5796178 C23.7731986,56.5796178 24.4,55.9508203 24.4,55.1751592 C24.4,54.3994982 23.7731986,53.7707006 23,53.7707006 C22.2268014,53.7707006 21.6,54.3994982 21.6,55.1751592 C21.6,55.9508203 22.2268014,56.5796178 23,56.5796178 Z M23,57.7796178 C21.563204,57.7796178 20.4,56.6127094 20.4,55.1751592 C20.4,53.7376091 21.563204,52.5707006 23,52.5707006 C24.436796,52.5707006 25.6,53.7376091 25.6,55.1751592 C25.6,56.6127094 24.436796,57.7796178 23,57.7796178 Z" id="Oval-10"></path>
                </g>
            </g>
        </svg>
        <div className={styles['device-status-wrapper']}>
          {renderDeviceStatus()}
        </div>
      </div>
    )
  }
}

export default Tablet;
