import React, { Component } from 'react';
import styles from './Checkmark.sass'

class Checkmark extends Component {

  render() {
    return (
      <div data-test-id={this.props.testID || 'checkmark-icon'} className={styles['checkmark-icon']}>
        <svg width="46px" height="35px" viewBox="0 0 46 35">
            <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Extensions/Open" transform="translate(-165.000000, -196.000000)" fill="#7ED321">
                    <g id="Open">
                        <g id="Desktop" transform="translate(77.000000, 156.000000)">
                            <g id="Checkmark" transform="translate(88.000000, 39.000000)">
                                <path d="M18.738387,22.1158415 L9.02180533,13.6693459 C7.76807776,12.5794972 5.88059149,12.7137477 4.79644857,13.9609115 L1.67536242,17.5513104 C0.590621274,18.7991623 0.725233203,20.6899878 1.97469039,21.7761243 L17.5117735,35.2823046 C17.9268505,35.6431256 18.411394,35.8697728 18.9132527,35.966281 C19.9373772,36.1742669 21.0422279,35.8384724 21.7783958,34.9916082 L44.3243047,9.05550677 C45.4120948,7.80414747 45.2822328,5.91161278 44.035069,4.82746987 L40.4446701,1.70638371 C39.1968182,0.621642569 37.3049496,0.757454441 36.2175263,2.00839183 L18.738387,22.1158415 Z"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
      </div>
    )
  }
}

export default Checkmark;
