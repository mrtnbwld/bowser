import React, { Component } from 'react';
import styles from './Arrow.sass'

class Arrow extends Component {

  render() {
    let size = this.props.size || 'large'
    let active = this.props.active || false
    return (
      <div data-test-id={this.props.testID || 'arrow-icon'} className={styles['arrow-icon-' + size] + ' ' + (active ? styles['active'] : '')}>
        <svg width="14px" height="21px" viewBox="0 0 14 21" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
            <g id="Assets" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Icons" transform="translate(-103.000000, -113.000000)" fill={this.props.color || 'white'}>
                    <g id="Arrow-back" transform="translate(0.000000, 82.000000)">
                        <path d="M103.297725,40.4795302 L112.479529,31.297726 C112.872976,30.904279 113.505683,30.8990823 113.898898,31.2922979 L116.020491,33.4138905 C116.41094,33.8043396 116.410027,34.4382956 116.015063,34.8332599 L109.656394,41.1919289 L116.015063,47.550598 C116.40851,47.944045 116.413706,48.5767518 116.020491,48.9699674 L113.898898,51.09156 C113.508449,51.4820091 112.874493,51.4810961 112.479529,51.0861319 L103.297725,41.9043277 C103.100098,41.7067014 103.000428,41.44871 103,41.1910249 C103.000368,40.9366004 103.099338,40.677917 103.297725,40.4795302 Z"></path>
                    </g>
                </g>
            </g>
        </svg>
      </div>
    )
  }
}

export default Arrow;
