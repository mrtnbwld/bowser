import React, { Component } from 'react';
import styles from './Confirm.sass'

class Confirm extends Component {

  render() {
    return (
      <div data-test-id={this.props.testID || 'confirm-icon'} className={styles['confirm-icon']}>
        <svg width="96px" height="96px" viewBox="132 192 96 96">
            <defs>
                <circle id="path-1" cx="39" cy="39" r="39"></circle>
                <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="-9" y="-9" width="96" height="96">
                    <rect x="-9" y="-9" width="96" height="96" fill="white"></rect>
                    <use xlinkHref="#path-1" fill="black"></use>
                </mask>
            </defs>
            <g id="Done!" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(141.000000, 201.000000)">
                <g id="Oval">
                    <use fill="#00AAFF" fillRule="evenodd" xlinkHref="#path-1"></use>
                    <use id={styles['faded-circle']} stroke="#E6F7FF" mask="url(#mask-2)" strokeWidth="18" xlinkHref="#path-1"></use>
                </g>
                <path d="M35.0265458,41.7199098 L28.0602325,35.664186 C26.9771951,34.722716 25.3350785,34.837739 24.3929564,35.9215265 L22.6386696,37.9396025 C21.6950696,39.0250902 21.8109612,40.6653415 22.8942198,41.6070038 L30.8879083,48.5558112 C31.0353755,48.9475599 31.2799089,49.309528 31.6184355,49.6038046 L33.6365115,51.3580913 C34.2206692,51.8658919 34.9650972,52.067308 35.674619,51.9765109 C36.4417338,52.0123934 37.2174201,51.7095593 37.7605041,51.0848127 L39.5147908,49.0667366 C39.8190163,48.7167652 40.01311,48.3091274 40.1001593,47.8858276 L53.8594299,32.0575974 C54.8032461,30.971861 54.6828417,29.335935 53.5990542,28.3938129 L51.5809781,26.6395262 C50.4954905,25.6959262 48.8565981,25.8102545 47.9166121,26.8915847 L35.0265458,41.7199098 Z" id={styles['checkmark']} fill="#FFFFFF"></path>
            </g>
        </svg>
      </div>
    )
  }
}

export default Confirm;
