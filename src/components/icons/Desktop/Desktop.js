import React, { Component } from 'react';
import styles from './Desktop.sass'
import Checkmark from '../Checkmark'
import Lock from '../Lock'
import Stop from '../Stop'

class Desktop extends Component {

  render() {
    let size = this.props.size || 'large',
        color

    this.props.clientColor && (color = 'clientcolor')

    let renderDeviceStatus = () => {
      switch(this.props.status){
        case 'paused': return <Lock clientColorStatus={this.props.clientColorStatus || false}/>
        case 'inactive': return <Stop/>
        default:
          return <Checkmark clientColorStatus={this.props.clientColorStatus || false}/>
      }
    }

    return (
      <div data-test-id={this.props.testID || 'desktop-icon'} className={styles['desktop-icon-' + size] + ' ' + styles[color]}>
        <svg width="85px" height="48px" viewBox="13 32 85 48">
            <defs>
                <path d="M84.1210538,2.74285714 L84.1994503,2.74285714 C84.6395043,2.74285714 85,2.38105288 85,1.9347447 L85,0.808112444 C85,0.358580671 84.6415817,0 84.1994503,0 L0.800549652,0 C0.360495681,0 0,0.361804265 0,0.808112444 L0,1.9347447 C0,2.38427647 0.358418288,2.74285714 0.800549652,2.74285714 L0.878987872,2.74285714 C1.01865355,2.9784139 1.27366978,3.13469388 1.56511597,3.13469388 L83.434884,3.13469388 C83.7282961,3.13469388 83.9820567,2.97764787 84.1210538,2.74285714 Z" id="path-1"></path>
            </defs>
            <g id="Device-icon---Active" stroke="none" strokeWidth="1" fill="black" fillRule="evenodd" transform="translate(13.000000, 32.000000)">
                <path d="M7.74324324,2.39639282 L7.74324324,44.6240153 C7.74324324,44.8443508 7.91938019,45.0204082 8.13901347,45.0204082 L77.2438694,45.0204082 C77.462076,45.0204082 77.6396396,44.8428232 77.6396396,44.6240153 L77.6396396,2.39639282 C77.6396396,2.17605734 77.4635027,2 77.2438694,2 L8.13901347,2 C7.92080691,2 7.74324324,2.17758497 7.74324324,2.39639282 Z M5.74324324,2.39639282 C5.74324324,1.07290161 6.81635124,0 8.13901347,0 L77.2438694,0 C78.5670168,0 79.6396396,1.0704327 79.6396396,2.39639282 L79.6396396,44.6240153 C79.6396396,45.9475066 78.5665316,47.0204082 77.2438694,47.0204082 L8.13901347,47.0204082 C6.81586611,47.0204082 5.74324324,45.9499755 5.74324324,44.6240153 L5.74324324,2.39639282 Z" id="Screen"></path>
                <ellipse id="Cam" cx="42.6914414" cy="4.11428571" rx="0.957207207" ry="0.979591837"></ellipse>
                <g id="Body" transform="translate(0.000000, 43.885714)">
                    <mask id="mask-2" fill="white">
                        <use xlinkHref="#path-1"></use>
                    </mask>
                    <use xlinkHref="#path-1"></use>
                    <rect id="Open/Close" fill="#FFFFFF" opacity="0.300000012" mask="url(#mask-2)" x="31.8156425" y="-0.914285714" width="21.3687151" height="2.28571429" rx="1.14285714"></rect>
                </g>
            </g>
        </svg>
        <div className={styles['device-status-wrapper'] + ' ' + styles[color]}>
          {renderDeviceStatus()}
        </div>
      </div>
    )
  }
}

export default Desktop;
