import React, { Component } from 'react';
import styles from './Stop.sass'

class Stop extends Component {

  render() {
    return (
      <div data-test-id={this.props.testID || 'stop-icon'} className={styles['stop-icon']}>
        <svg width="35px" height="35px" viewBox="5 17 35 35">
            <rect id="Rectangle-9" stroke="none" fill="#000000" fillRule="evenodd" x="5" y="17" width="35" height="35" rx="1"></rect>
        </svg>
      </div>
    )
  }
}

export default Stop;
