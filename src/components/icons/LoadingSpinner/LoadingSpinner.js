import React from 'react'
// import clientConfig from '../../clientConfig'
import styles from './LoadingSpinner.sass'

class LoadingSpinner extends React.Component {

  render(){
    return (
      <div data-test-id={this.props.testID || 'loadingspinner-icon'} className={styles['loading-container']}>
          <div className={
            this.props.white ? (
                styles['loading-spinner'] + ' ' + styles['is-white'])
              : styles['loading-spinner']
          }></div>
      </div>
    )
  }
}

export default LoadingSpinner
