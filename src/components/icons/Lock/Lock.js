import React, { Component } from 'react';
import styles from './Lock.sass'

class Lock extends Component {

  render() {
    return (
      <div data-test-id={this.props.testID || 'lock-icon'} className={styles['lock-icon']}>
        <svg width="37px" height="46px" viewBox="0 0 37 46">
            <path d="M18.5,30.5 C26.2806847,30.5 32.5,23.7398747 32.5,15.5 C32.5,7.26012534 26.2806847,0.5 18.5,0.5 C10.7193153,0.5 4.5,7.26012534 4.5,15.5 C4.5,23.7398747 10.7193153,30.5 18.5,30.5 Z M18.5,25.5 C13.5781354,25.5 9.5,21.0672441 9.5,15.5 C9.5,9.93275592 13.5781354,5.5 18.5,5.5 C23.4218646,5.5 27.5,9.93275592 27.5,15.5 C27.5,21.0672441 23.4218646,25.5 18.5,25.5 Z" id="Oval" stroke="none" fillRule="evenodd"></path>
            <rect id="Rectangle-6" stroke="none" fillRule="evenodd" x="0" y="14" width="37" height="32" rx="2"></rect>
        </svg>
      </div>
    )
  }
}

export default Lock;
