import React, { Component } from 'react';
import styles from './LongArrow.sass'

class LongArrow extends Component {

  render() {
    return (
      <div data-test-id={this.props.testID || 'long-arrow-icon'} className={styles['long-arrow-icon']}>
        <svg width="21px" height="17px" viewBox="0 0 21 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
            <g id="Style-guide" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Icons" transform="translate(-101.000000, -610.000000)" fill="#FFFFFF">
                    <g id="Background">
                        <path d="M115.479261,610.581767 L121.463068,617.097554 C121.821023,617.457474 122,617.947709 122,618.487589 C122,619.027469 121.821023,619.511498 121.463068,619.877624 L115.479261,626.418233 C114.769318,627.193922 113.611932,627.193922 112.901989,626.418233 C112.192045,625.642544 112.192045,624.389031 112.901989,623.613342 L115.777557,620.485764 L102.831534,620.485764 C101.81733,620.485764 101,619.598376 101,618.5 C101,617.401624 101.81733,616.514236 102.825568,616.514236 L115.771591,616.514236 L112.896023,613.386658 C112.18608,612.610969 112.18608,611.357456 112.896023,610.581767 C113.611932,609.806078 114.763352,609.806078 115.479261,610.581767 Z" id="Shape-Copy"></path>
                    </g>
                </g>
            </g>
        </svg>
      </div>
    )
  }
}

export default LongArrow;
