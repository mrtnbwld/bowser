import React from 'react'
import styles from './MenuIcon.sass'
import clientConfig from '../../../clientConfig'
import classNames from 'classnames/bind'
import {enoughContrast} from '../../../utils/colorAndContrast'
// color check
let cx = classNames.bind(styles);


class MenuIcon extends React.Component {
  render(){
    let buttonStyling = cx({
      'menu-icon': true,
      'menu-icon-closed': this.props.isToggled,
      // inProgress: this.props.store.submissionInProgress,
      // error: this.props.store.errorOccurred,
      // disabled: this.props.form.valid,
    })

    let barStyling = cx({
      'menu-icon-bar': true,
      'menu-icon-bar-branded': enoughContrast(clientConfig.brandColor) && !this.props.isToggled
    })


    // [styles['menu-icon']]

    // if(this.props.isToggled){
    //   styling.push(styles['menu-icon-closed'])
    // console.log(styling)
    // }

    return (
      <div onClick={this.props.onClick} className={buttonStyling}>
        <div className={barStyling}></div>
      </div>
    )
  }
}

MenuIcon.defaultProps = {
  isToggled: false
}

export default MenuIcon
