import React, { Component } from 'react'
import styles from './SupportPage.sass'
import {I18n} from 'react-redux-i18n'
import clientConfig from '../../clientConfig'
import ExternalLink from '../ExternalLink'

class SupportPage extends Component {
  render() {
    return (
      <div data-test-id='privacy-page' className={styles['privacy-page']}>
        <h2 data-test-id='privacy-header' className={styles['page-header']}>
          {I18n.t('supportPage.header')}
        </h2>
        <p data-test-id='privacy-message' className={styles['page-message']}>
          {I18n.t('supportPage.message')}
        </p>
        <ExternalLink
          href={clientConfig.supportUrl}
          text={I18n.t('supportPage.actions.view')}
          className={styles['link']}
          dataTestId={'support-link'}
        />
      </div>
    )
  }
}

export default SupportPage
