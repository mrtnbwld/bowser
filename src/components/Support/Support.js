import React, { Component } from 'react'
import styles from './Support.sass'
import WarningCard from '../WarningCard'
import Button from '../Button'
import {I18n} from 'react-redux-i18n'

class Support extends Component {
  constructor(props){
    super(props)

    this.state = {
      isSending: false,
      form: {
        email: undefined,
        subject: 'general',
        message: undefined
      },
      validationErrors: {
        email: false,
        message: false
      },
      warning: undefined
    }
  }

  componentWillReceiveProps(nextProps){
    let {warnings} = nextProps || []
    let warning = warnings.filter((w) => {return w.name === 'couldNotSendMessage'})[0]

    if (warning){
      this.setState({
        warning: warning,
        isSending: false
      })
    }
  }

  _resetErrors(){
    this.setState({
      validationErrors: {
        email: false,
        message: false
      },
      warning: undefined
    })
  }

 _handleSubmit(e){
    this._resetErrors()
    const validationSet = this._validateForm()
    if(validationSet.valid){
      this.setState({
        isSending: true
      })
      this.props.submitForm({
        ...this.state.form,
      }, this.state.form.email)
    }else{
      this.setState({
        validationErrors: validationSet.errors
      })
    }
  }

  _validateForm(){
    let {email, message} = this.state.form
    let validationSet = {
      valid: true,
      errors: {}
    }

    email = email && email.trim()
    if(email === undefined || email === ''){
      validationSet.errors.email = 'isBlank'
    }else if(!/^(([^<>()[]\\.,;:\s@"]+(\.[^<>()[]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)){
      validationSet.errors.email = 'isNotEmail'
    }

    if(message === undefined || message === ''){
      validationSet.errors.message = 'isBlank'
    }

    validationSet.valid = Object.keys(validationSet.errors).length <= 0
    return validationSet
  }

  _handleChange(inputName, value){
    var form = this.state.form
    form[inputName] = value
    this.setState({form})
  }

  render() {
    let toRender
    let validationErrors = this.state.validationErrors

    if(this.state.warning){
      toRender = <WarningCard action={() => this._resetErrors()} error={this.state.warning}/>
    } else {
      toRender = (
        <div data-test-id='support-page' className={styles['support-page']}>
          <p data-test-id='support-header' className={styles['page-header']}>{I18n.t('support.header')}</p>
          <form data-test-id='support-form'>
            <label
              data-test-id='support-form-email-label'
              className={styles['input-label']}
              htmlFor='email'>{I18n.t('support.form.email.label')}</label>
            <input
              data-test-id='support-form-email-input'
              type='email'
              id='email'
              value={this.state.form.email}
              className={this.state.validationErrors.email && styles['invalid']}
              placeholder={I18n.t('support.form.email.placeholder')}
              onChange={(e) => this._handleChange('email', e.target.value)}/>
            {(validationErrors && validationErrors.email) && <span className={styles['error']}>{I18n.t('support.form.errors.email')}</span>}

            <label
              data-test-id='support-form-subject-label'
              className={styles['input-label']}
              htmlFor='subject'>{I18n.t('support.form.subject.label')}</label>
            <select
              data-test-id='support-form-subject-select'
              defaultValue={this.state.form.subject}
              id='subject'
              placeholder={I18n.t('support.form.subject.placeholder')}
              onChange={(e) => this._handleChange('subject', e.target.value)}>
              <option data-test-id='support-form-subject-option' value='general'>{I18n.t('support.form.subject.options.general')}</option>
            </select>

            <label
              data-test-id='support-form-message-label'
              className={styles['input-label']}
              htmlFor='message'>{I18n.t('support.form.message.label')}</label>
            <textarea
              data-test-id='support-form-message-input'
              value={this.state.form.message}
              className={this.state.validationErrors.message && styles['invalid']}
              onChange={(e) => this._handleChange('message', e.target.value)}>
            </textarea>
            {(validationErrors && validationErrors.message) && <span className={styles['error']}>{I18n.t('support.form.errors.message')}</span>}

            <Button
              inProgress={this.state.isSending}
              label={I18n.t('support.form.actions.send')}
              progressLabel={I18n.t('support.form.actions.sending')}
              action={() => this._handleSubmit()}/>
          </form>
        </div>
      )
    }

    return toRender
  }
}

export default Support;
