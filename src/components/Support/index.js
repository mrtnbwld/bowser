import Support from './Support'
import SupportPage from './SupportPage'

export default Support

export {
  Support, SupportPage
}
