import React, { Component } from 'react'
import styles from './WarningCard.sass'
import Bell from '../icons/Bell'
import Button from '../Button'
import {I18n} from 'react-redux-i18n'

class WarningCard extends Component {
  render() {
    let {priority, name, instructions, action} = this.props.error

    return (
      <div data-test-id='warning' className={styles['warning']}>
        <div data-test-id='warning-label' className={styles['warning-label']}>
          <Bell priority={this.props.error.priority}/>
          <div>
            <h2 data-test-id='warning-title' className={styles['warning-title']}>{I18n.t('errors.' + name + '.title')}</h2>
            {priority &&
              <p data-test-id='warning-priority' className={styles['warning-subtitle']}>{I18n.t('errors.labels.' + priority)}</p>
            }
          </div>
        </div>
        <p data-test-id='warning-description' className={styles['warning-description']}>
          {I18n.t('errors.' + name + '.message')}
        </p>
        {instructions &&
          <div data-test-id='warning-instructions' className={styles['warning-instructions']}></div>
        }
        {action &&
          <Button action={() => this.props.action()} label={I18n.t('errors.' + name + '.action')}/>
        }
      </div>
    )
  }
}

export default WarningCard;
