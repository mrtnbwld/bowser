import React, { Component } from 'react'
import styles from './UserIdentification.sass'
import {I18n} from 'react-redux-i18n'
import Button from '../Button'
import {DisneyButton} from '../Button'
import Logo from '../../assets/whitelabel/logo.png'
import {connection} from '../../utils/CoreConnection'
import {camelize} from '../../utils/camelize'
import clientConfig from '../../clientConfig'
import Cancel from '../icons/Cancel'

class UserIdentification extends Component {
  constructor(props){
    super(props)
    this.state = {
      buttons: []
    }
  }

  componentWillMount(){
    connection.getUserIdentificationButtons((buttons = {})=>{
      this.setState({
        buttons: buttons.data || []
      })
    })
  }

  render() {
    let buttons = this.state.buttons

    return (
      <div className={styles['user-identification-page']}>
        <div className={styles['simple-navigation']}>
          <img alt='logo' className={styles['logo']} src={Logo}/>
          <Cancel onClick={() => connection.closePanel()}/>
        </div>
        <div className={styles['page-content']}>
          <h3>{I18n.t('userIdentificationDialog.title')}</h3>
          { clientConfig.clientCode === "cmb"
            ?
              buttons.map((b, i) => <DisneyButton key={b.key} identifier={b.key} action={() => connection.setCurrentUser(b.value)} />)
            :
              buttons.map((b, i) => {
                return <Button key={i} action={() => connection.setCurrentUser(b.value)} label={I18n.t(`userIdentification.buttons.${camelize(b.key)}`)}/>
              })
          }
        </div>
      </div>
    )
  }
}

export default UserIdentification
