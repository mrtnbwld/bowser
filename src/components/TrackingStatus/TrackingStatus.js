import React from 'react'
import PropTypes from 'prop-types'
import Resume from '../icons/Resume'
import Pause from '../icons/Pause'
import moment from 'moment'
import Desktop from '../icons/Desktop'
import {I18n} from 'react-redux-i18n'
import styles from './TrackingStatus.sass'


const TrackingStatus = ({ trackerPaused, pausedUntil, buttonActionPaused, buttonActionTracking}) => {
  const trackerStatusLabel = trackerPaused ? 'paused' : 'active'
  const buttonLabel        = trackerPaused ? I18n.t('trackerStatus.actions.resume') : I18n.t('trackerStatus.actions.pause')
  const buttonAction       = trackerPaused ? buttonActionPaused : buttonActionTracking

  return (
    <div className={styles['container']}>
      <div className={styles['graphic']}>
        <Desktop clientColor={true} status={trackerStatusLabel}/>
        <div className={styles['tracker-status']}>
          <span
            data-test-id='tracker-status-label'
            className={styles['tracker-status-label']}>
            {I18n.t('trackerStatus.label.' + trackerStatusLabel)}
          </span>
          {
            trackerPaused ?
            (
              <div className={styles['tracker-status-message']} data-test-id='tracker-status-paused'>
                <span>
                  {I18n.t('trackerStatus.message.resumeAt', {time: moment(pausedUntil).format('LT')})}
                </span>
              </div>
            )
            : undefined
          }
        </div>
      </div>

      <div onClick={buttonAction} className={styles['pause-resume']}>
        {
          trackerPaused ?
          <Resume/>
          :
          <Pause/>
        }
        <button data-test-id='pause-tracking-button' className={styles['pause-tracking-button']}>{buttonLabel}</button>
      </div>
    </div>
  )
}

TrackingStatus.propTypes = {
  trackerPaused: PropTypes.bool,
  pausedUntil: PropTypes.object,
  buttonActionPaused: PropTypes.func.isRequired,
  buttonActionTracking: PropTypes.func.isRequired,
}

export default TrackingStatus
