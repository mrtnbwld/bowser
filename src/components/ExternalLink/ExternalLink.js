import React from 'react';
import PropTypes from 'prop-types'

const ExternalLink = props => {
  const ua = navigator.userAgent
  const is_safari = ua.includes("Safari") && !ua.includes("Chrome");

  if(is_safari){
    return(
      <a
        data-test-id={props.dataTestId}
        href={props.href}
        rel="noopener noreferrer"
        className={props.className}
        >
        {props.text}
      </a>
    )
  }else{
    return(
      <a
        data-test-id={props.dataTestId}
        href={props.href}
        rel="noopener noreferrer"
        target='_blank'
        className={props.className}
        >
          {props.text}
      </a>
    )
  }
}

ExternalLink.propTypes = {
  text: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
  dataTestId: PropTypes.string
}

export default ExternalLink;
