import React, { Component } from 'react'
import styles from './Privacy.sass'
import {I18n} from 'react-redux-i18n'
import clientConfig from '../../clientConfig'
import ExternalLink from '../ExternalLink'

class Privacy extends Component {
  render() {
    return (
      <div data-test-id='privacy-page' className={styles['privacy-page']}>
        <h2 data-test-id='privacy-header' className={styles['page-header']}>{I18n.t('privacy.header')}</h2>
        <p data-test-id='privacy-message' className={styles['page-message']}>{I18n.t('privacy.message')}</p>
        <ExternalLink
          href={clientConfig.privacyUrl}
          text={I18n.t('privacy.actions.view')}
          className={styles['link']}
          dataTestId={'privacy-policy-link'}
        />
      </div>
    )
  }
}

export default Privacy
