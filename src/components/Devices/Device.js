import React, { Component } from 'react'
import styles from './Devices.sass'
import moment from 'moment'
import { Link } from 'react-router'
import Desktop from '../icons/Desktop'
import Smartphone from '../icons/Smartphone'
import Tablet from '../icons/Tablet'
import Arrow from '../icons/Arrow'
import {I18n} from 'react-redux-i18n'

class Device extends Component {
  constructor(props){
    super(props)
    this.state = {
      showDetails: false
    }
  }

  render() {
    let renderDeviceKind = () => {
      let kind = this.props.device.kind || undefined
      switch(kind){
        case 'smartphone':
          return <Smartphone status={this.props.device.status} size='small' color='black'/>
        case 'tablet':
          return <Tablet status={this.props.device.status} size='small' color='black'/>
        case 'desktop':
        case 'chrome_plugin':
        case 'firefox_plugin':
        case 'safari_plugin':
          return <Desktop status={this.props.device.status} size='small' color='black'/>
        default: return false
      }
    }

    return (
      <div data-test-id={'device-'} className={styles['device']}>
        <div className={styles['device-details']} onClick={() => this.setState({showDetails: !this.state.showDetails})}>
          {renderDeviceKind()}
          <div className={styles['device-details-info']}>
            <strong data-test-id='device-name'>{this.props.device.name}</strong>
            <span data-test-id='device-status' className={styles['device-status']}>{I18n.t('devices.status.' + this.props.device.status)}</span>
          </div>
          <Arrow size={'small'} active={this.state.showDetails} color={'black'}/>
        </div>
        <div className={styles['device-more-details'] + ' ' + (this.state.showDetails ? styles['active'] : '')}>
          <p>
            <span data-test-id='device-installation-header' className={styles['header']}>{I18n.t('devices.device.installation.header')}</span>
            { this.props.device.install_started_at && (
              <span data-test-id='device-installation-details' className={styles['details']}>
                {moment(this.props.device.install_started_at).format('lll')}
              </span>
            )}

            <span data-test-id='device-activity-header' className={styles['header']}>{I18n.t('devices.device.activity.header')}</span>
            { this.props.device.first_activity_at ? (
                <span data-test-id='device-first-activity' className={styles['details']}>
                  {moment(this.props.device.first_activity_at).format('lll')}
                </span>
              ) : (
                <span data-test-id='device-no-activity' className={styles['details']}>
                  {I18n.t('devices.device.activity.noActivity')}
                </span>
              )
            }
            <span data-test-id='device-support-header' className={styles['header']}>{I18n.t('devices.device.support.header')}</span>
            {/* The links below should point towards topics in the support center */}
            <span className={styles['details']}>
              <Link to="">{I18n.t('devices.device.support.installation')}</Link>, <Link to="">{I18n.t('devices.device.support.removal')}</Link>
            </span>
          </p>
        </div>
      </div>
    )
  }
}

export default Device;
