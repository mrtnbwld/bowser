import React, { Component } from 'react'
import styles from './Devices.sass'
import Device from './Device'
import {I18n} from 'react-redux-i18n'

class StoppedDevices extends Component {

  render() {
    let devices = this.props.devices || [],
        stoppedDevices = devices.filter((d) => { return d.status === 'inactive'})

    return (
      <div className={
        this.props.selected ?
          (styles['tab-stopped-devices'] + ' ' + styles['active'])
        : styles['tab-stopped-devices']
      }>
        {stoppedDevices.length >= 1 ? (
            <div>
              <span className={styles['header']}>{I18n.t('devices.tabs.stopped.stoppedDevices')}</span>
              {stoppedDevices.map((d, i) => {
                return <Device key={i} device={d}/>
              })}
            </div>
          ) : <span className={styles['no-stopped-devices']}>{I18n.t('devices.tabs.stopped.noStoppedDevices')}</span>
        }
      </div>
    )
  }
}

export default StoppedDevices;
