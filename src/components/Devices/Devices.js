import React, { Component } from 'react'
import styles from './Devices.sass'
import CurrentDevices from './CurrentDevices'
import StoppedDevices from './StoppedDevices'
import LoadingSpinner from '../icons/LoadingSpinner'
import WarningCard from '../WarningCard'
import {I18n} from 'react-redux-i18n'

class Devices extends Component {

  constructor(props){
    super(props)
    this.state = {
      selectedTab: 'current',
      isLoading: false,
      warning: undefined
    }
  }

  componentWillMount(){
    this._loadDevices()
  }

  _loadDevices(){
    this.setState({
      isLoading: true,
      warning: undefined
    })
    this.props.getDevicesList('qa1_diederick')
  }

  componentWillReceiveProps(nextProps){
    let {devices, warnings} = nextProps || []
    let warning = warnings.filter((w) => {return w.name === 'couldNotLoadDevices'})[0]

    if(devices.length){
      this.setState({isLoading: false})
    }
    if(warning){
      this.setState({warning: warning})
    }
  }

  render() {
    if(this.props.devices){
      return (
        <div data-test-id='devices-page' className={styles['devices']}>
          <nav className={styles['devices-tabs']}>
            <div
              data-test-id='tab-button-current'
              className={this.state.selectedTab === 'current' ? (
                styles['tab-button'] + ' ' + styles['active'])
              : styles['tab-button']}
              onClick={() => this.setState({selectedTab: 'current'})}>
                {I18n.t('devices.tabs.current.title')}
            </div>
            <div
              data-test-id='tab-button-stopped'
              className={this.state.selectedTab === 'stopped' ? (
                styles['tab-button'] + ' ' + styles['active'])
              : styles['tab-button']}
              onClick={() => this.setState({selectedTab: 'stopped'})}>
                {I18n.t('devices.tabs.stopped.title')}
            </div>
          </nav>

          <div className={styles['tabs-wrapper']}>
            <CurrentDevices selected={this.state.selectedTab === 'current'} devices={this.props.devices}/>
            <StoppedDevices selected={this.state.selectedTab === 'stopped'} devices={this.props.devices}/>
          </div>
        </div>
      )
    }

    if(this.state.isLoading) {
      return <div className={styles['loading']}><LoadingSpinner/></div>
    }

    if (this.state.warning) {
      return <WarningCard action={() => this._loadDevices()} error={this.state.warning}/>
    }
  }
}

export default Devices;
