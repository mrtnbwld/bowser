import React, { Component } from 'react'
import styles from './Devices.sass'
import { Link } from 'react-router'
import LongArrow from '../icons/LongArrow'
import DeviceInfo from '../../utils/DeviceInfo'
import Device from './Device'
import {I18n} from 'react-redux-i18n'

class CurrentDevices extends Component {

  render() {
    let getDevicesList = this.props.devices || [],
        thisDevice     = getDevicesList.filter((d) => { return d.id === DeviceInfo.id })[0],
        otherDevices   = getDevicesList.filter((d) => { return d.id !== DeviceInfo.id && d.status !== 'inactive'}) || []

    return (
      <div className={
        this.props.selected ?
          (styles['tab-current-devices'] + ' ' + styles['active'])
        : styles['tab-current-devices']
      }>

        {thisDevice && (
          <div>
            <span data-test-id='this-device-header' className={styles['header']}>{I18n.t('devices.tabs.current.thisDevice')}</span>
            <Device key='this-device' device={thisDevice}/>
          </div>
        )}

        <span data-test-id='current-devices-header' className={styles['header']}>{I18n.t('devices.tabs.current.otherDevices')}</span>
        {otherDevices.length >= 1 ? (
          <div>
            {otherDevices.map((d, i) => {
              return <Device key={i} device={d}/>
            })}
          </div>
          ) : <span data-test-id='no-other-devices'>{I18n.t('devices.tabs.current.noOtherDevices')}</span>
        }

        <Link to={'/add-device'} data-test-id='add-device-button' className='button arrow'>{I18n.t('devices.tabs.current.addDevice')}<LongArrow/></Link>
      </div>
    )
  }
}

export default CurrentDevices;
