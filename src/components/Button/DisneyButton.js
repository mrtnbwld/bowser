import React, { Component } from 'react'
import styles from './DisneyButton.sass'

class DisneyButton extends Component {
  render() {
    let disneyImages = {
      "child_user": require("../../assets/images/child-icon.png"),
      "parent_user": require("../../assets/images/parent-icon.png")
    }

    return (
      <div data-test-id='button' onClick={this.props.action} className={styles['button']}>
        <div className={styles['button-content']}>
          <img src={disneyImages[this.props.identifier]} alt={this.props.identifier}/>
        </div>
      </div>
    )
  }
}

export default DisneyButton
