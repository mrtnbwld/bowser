import React, { Component } from 'react'
import styles from './Button.sass'
// import LongArrow from '../icons/LongArrow'
import LoadingSpinner from '../icons/LoadingSpinner'

class Button extends Component {
  render() {
    let toRender

    this.props.inProgress ? (
      toRender = (
        <div className={styles['button-content']}>
          <LoadingSpinner white/>
          <span>{this.props.progressLabel}</span>
        </div>
      )
    ): (
      toRender = (
        <div className={styles['button-content']}>
          <span>{this.props.label}</span>
        </div>
      )
    )
    return (
      <div data-test-id='button' onClick={this.props.action} className={styles['button']}>
        {toRender}
      </div>
    )
  }
}

export default Button
