import React, { Component } from 'react';
import { Link } from 'react-router'
import styles from './Navigation.sass'
import Arrow from '../icons/Arrow'
import MenuIcon from '../icons/MenuIcon'
import Logo from '../../assets/whitelabel/logo.png'
import {I18n} from 'react-redux-i18n'

class Navigation extends Component {
  constructor(props){
    super(props)
    this.state = {
      menuToggled: false
    }
    this.getPageTitle     = this.getPageTitle.bind(this)
    this.shouldRenderLogo = this.shouldRenderLogo.bind(this)
  }

  getPageTitle(){
    const path = this.props.location.pathname
    switch(path){
      case '/support':
      case '/message-sent': return I18n.t('support.title')
      case '/my-devices': return I18n.t('devices.title')
      case '/privacy': return I18n.t('privacy.title')
      default: return undefined
    }
  }

  shouldRenderLogo(){
    const path = this.props.location.pathname
    switch(path){
      case '/':
      case '/error':
      case '/user-identification': return true
      default: return false
    }
  }

  displayMenuButton(){
    const path = this.props.location.pathname
    switch(path){
      case '/':
      case '/error':
      case '/user-identification': return true
      default: return false
    }
  }

  render() {
    return (
      <div data-test-id='navigation'>
        <div className={styles['navigation'] + ' ' + (this.shouldRenderLogo() && styles['white'])}>
          {this.shouldRenderLogo()
            ? <img alt='logo' className={styles['logo']} src={Logo}/>
            : <Link className={styles['back-button']} to='/'><Arrow/></Link>
          }

          {this.getPageTitle()
            && <h1 className={styles['page-title']}>{this.getPageTitle()}</h1>
          }

          {(this.displayMenuButton() && this.props.displayMenuButton) ?
            <MenuIcon
              isToggled={this.state.menuToggled}
              onClick={() => this.setState({menuToggled: !this.state.menuToggled})}/>
            : <div/>
          }

        </div>
        {/* todo: move to seperate component*/}
        <nav
          data-test-id='navigation-overlay'
          className={
          styles['navigation-overlay'] + ' ' +
          (this.state.menuToggled && styles['visible'])}>
          <div>
            <Link
              data-test-id='navigation-to-privacy'
              onClick={() => this.setState({menuToggled: !this.state.menuToggled})}
              to={'/privacy'}>
                {I18n.t('menu.privacy')}
              </Link>
            <Link
              data-test-id='navigation-to-support'
              onClick={() => this.setState({menuToggled: !this.state.menuToggled})}
              to={'/support'}>
                {I18n.t('menu.support')}
              </Link>
          </div>
        </nav>
      </div>
    )
  }
}

Navigation.defaultProps = {
  displayMenuButton: true
}

export default Navigation;
