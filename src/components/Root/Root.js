import React, { Component } from 'react';
import clientConfig from '../../clientConfig'
require('./Root.sass')

class Root extends Component {
  componentWillMount() {
    this.setClientColor()
  }

  setClientColor(){
    // When we're building the plugin for Edge, we should check CSS variables again, as it is still on the roadmap for this browser
    document.body.style.setProperty('--client-maincolor', clientConfig.brandColor)
  }

  render() {
    return (
      <div data-test-id='root'>{this.props.children}</div>
    )
  }
}

export default Root;
