import React, { Component } from 'react'
import TrackingStatus from '../TrackingStatus'
import KillSwitch from '../KillSwitch'
import LoadingSpinner from '../icons/LoadingSpinner'
import moment from 'moment'
import {I18n} from 'react-redux-i18n'
import clientConfig from '../../clientConfig'

class Home extends Component {

  constructor(props){
    super(props)
    moment.locale('en-gb')
  }

  _handlePause(){
    const duration = 900
    this.props.pauseTracker(duration)
  }

  _handleResume(){
    this.props.unpauseTracker()
  }

  componentWillMount(){
    this.checker = setInterval(this.props.getTrackerState, 5000)
  }

  componentWillUnmount() {
    clearInterval(this.checker)
  }

  render() {
    const {deviceRemoved} = this.props.participation
    const {pausedUntil, paused} = this.props.tracker

    if(this.props.participation.loading){
      return(<div data-test-id='home-page'>
        <LoadingSpinner/>
      </div>)
    }

    return (
      <div data-test-id='home-page'>
        {
          deviceRemoved
            ? <KillSwitch
                title={I18n.t("killswitch.deviceRemoved.title", {appName: clientConfig.appName})}
                text={<p>{I18n.t("killswitch.deviceRemoved.message", {appName: clientConfig.appName})}</p>}
              />
            : <TrackingStatus
                trackerPaused={paused}
                pausedUntil={pausedUntil}
                buttonActionPaused={this._handleResume.bind(this)}
                buttonActionTracking={this._handlePause.bind(this)}
              />
        }

      </div>
    )
  }
}

export default Home
