import React, { Component } from 'react'
import styles from './MessageSent.sass'
import Confirm from '../icons/Confirm'
import {I18n} from 'react-redux-i18n'

class MessageSent extends Component {
  render() {
    return (
      <div data-test-id='message-sent-page' className={styles['message-sent-page']}>
        <Confirm/>
        <h2 data-test-id='message-sent-header' className={styles['page-header']}>{I18n.t('messageSent.header')}</h2>
        <p data-test-id='message-sent-message' className={styles['page-message']}>{I18n.t('messageSent.message')}</p>
      </div>
    )
  }
}

export default MessageSent
