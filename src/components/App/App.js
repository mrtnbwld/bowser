import React, { Component } from 'react';
import PropTypes from 'prop-types'
import styles from './App.sass'
import Navigation from '../Navigation'
import LoadingSpinner from '../icons/LoadingSpinner'

class App extends Component {
  componentWillMount() {
    this.props.getTrackerState()
    this.props.getParticipationStatus()
  }

  render() {
    if(this.props.appStarted){
      return (
        <div data-test-id='app' className={styles['app']}>
          <Navigation location={this.props.location}/>
          {this.props.children}
        </div>
      )
    }else{
      return(<div data-test-id='home-page'>
        <LoadingSpinner/>
      </div>)
    }
  }
}

App.propTypes = {
  getParticipationStatus: PropTypes.func.isRequired
}

export default App;
