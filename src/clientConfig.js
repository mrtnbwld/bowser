import settings from './clientConfig.json'

const {
  clientCode,
  defaultLocale,
  pluginName,
  mainColor,
  privacyUrl,
  support,
  prefered_sub_domain,
  supportUrl,
  prefered_domain
} = settings

const rootDomain = `https://${prefered_sub_domain}.${prefered_domain}/`

let clientConfig = {
  clientCode: clientCode, // "cmb"
  defaultLocale: defaultLocale,
  appName: pluginName || 'Wakoopa',
  brandColor: mainColor || '#ff6d00',
  privacyUrl: privacyUrl,
  rootDomain: rootDomain,
  supportUrl: supportUrl || `${rootDomain}frontend/profile#support-popup`,
  supportAddress: support
}

export default clientConfig
