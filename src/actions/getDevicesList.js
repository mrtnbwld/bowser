import {
  REQUEST_DEVICES_LIST
} from '../actions'

export function getDevicesList(id){
  return {
    type: REQUEST_DEVICES_LIST,
    payload:{
      endpoint: `api/v1/participants/${id}/devices`,
      method: 'GET'
    }
  }
}
