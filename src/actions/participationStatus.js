import {
  REQUEST_PARTICIPATION_STATUS,
  DEVICE_AUTH_HASH
} from '../actions'

export function getParticipationStatus(deviceAuthHash){
  return {
    type: REQUEST_PARTICIPATION_STATUS,
    payload:{
      endpoint: `api/v1/devices/${deviceAuthHash}/participation_status`,
      method: 'GET'
    }
  }
}

export function deviceAuthHash(deviceAuthHash){
  return {
    type: DEVICE_AUTH_HASH,
    payload: {
      deviceAuthHash
    }
  }
}
