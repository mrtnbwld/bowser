export const
REQUEST_DEVICES_LIST                 = 'REQUEST_DEVICES_LIST',
DISABLE_DEVICE                       = 'DISABLE_DEVICE',
ADD_DEVICE                           = 'ADD_DEVICE',
FETCHED_DEVICES_LIST_START           = 'FETCHED_DEVICES_LIST_START',
FETCHED_DEVICES_LIST_SUCCESS         = 'FETCHED_DEVICES_LIST_SUCCESS',
FETCHED_DEVICES_LIST_ERROR           = 'FETCHED_DEVICES_LIST_ERROR',
FETCHED_DEVICES_LIST_END             = 'FETCHED_DEVICES_LIST_END',
SUBMIT_SUPPORT_FORM                  = 'SUBMIT_SUPPORT_FORM',
REQUEST_SUPPORT_FORM                 = 'REQUEST_SUPPORT_FORM',
FETCHED_SUPPORT_FORM_START           = 'FETCHED_SUPPORT_FORM_START',
FETCHED_SUPPORT_FORM_SUCCESS         = 'FETCHED_SUPPORT_FORM_SUCCESS',
FETCHED_SUPPORT_FORM_ERROR           = 'FETCHED_SUPPORT_FORM_ERROR',
FETCHED_SUPPORT_FORM_END             = 'FETCHED_SUPPORT_FORM_END',
ADD_ERROR                            = 'ADD_ERROR',
REMOVE_ERROR                         = 'REMOVE_ERROR',
ADD_WARNING                          = 'ADD_WARNING',
REMOVE_WARNING                       = 'REMOVE_WARNING',
TRACKER_STATE                        = 'TRACKER_STATE',
PAUSE_TRACKER                        = 'PAUSE_TRACKER',
UNPAUSE_TRACKER                      = 'UNPAUSE_TRACKER',
DEVICE_AUTH_HASH                     = 'DEVICE_AUTH_HASH',
REQUEST_PARTICIPATION_STATUS         = 'REQUEST_PARTICIPATION_STATUS',
FETCHED_PARTICIPATION_STATUS_START   = 'FETCHED_PARTICIPATION_STATUS_START',
FETCHED_PARTICIPATION_STATUS_SUCCESS = 'FETCHED_PARTICIPATION_STATUS_SUCCESS',
FETCHED_PARTICIPATION_STATUS_ERROR   = 'FETCHED_PARTICIPATION_STATUS_ERROR',
FETCHED_PARTICIPATION_STATUS_END     = 'FETCHED_PARTICIPATION_STATUS_END',
APP_START_FINISHED                   = 'APP_START_FINISHED'
