import {
  SUBMIT_SUPPORT_FORM
} from './index'

export function submitForm(data, email){
  const body = formDataToMessage(data)

  return {
    type: SUBMIT_SUPPORT_FORM,
    payload: {
      body: body,
      email: email.trim()
    }
  }
}

export function formDataToMessage(data){
  var message = ''
  Object.keys(data).forEach(key => {
    message += `${key}: ${data[key]}\n`
  })
  return message
}
