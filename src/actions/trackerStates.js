import {
  TRACKER_STATE,
  PAUSE_TRACKER,
  UNPAUSE_TRACKER
} from '../actions'

export const getTrackerState = () => {
  return {
    type: TRACKER_STATE
  }
}

export const pauseTracker = (duration) => {
  return {
    type: PAUSE_TRACKER,
    payload: {
      duration: duration
    }
  }
}

export const unpauseTracker = () => {
  return {
    type: UNPAUSE_TRACKER
  }
}

