import tracker from '../mock/tracker';

export function initialize() {
  window.$Wakoopa = tracker;
  // console.log('MOCK TRACKER INITIALIZED');
}

export default {
  initialize: initialize
};
