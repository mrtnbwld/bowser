export function enoughContrast(hexColor1, hexColor2 = "#ffffff"){
  if(!hexColor1)
    return false

  const threshold     = 2
  const hex1RGB       = hexToRGB(hexColor1)
  const hex2RGB       = hexToRGB(hexColor2)
  const contrastRatio = (luminanace(hex2RGB) + 0.05) / (luminanace(hex1RGB) + 0.05)

  return contrastRatio > threshold
}

export function colorBasedOnContrast(hexColor1, hexColor2, hexBackupColor){
  if( enoughContrast(hexColor1, hexColor2) ){
    return hexColor1
  }else{
    return hexBackupColor
  }
}

function hexToRGB(hex){
  const cutHex = (h) => (h.charAt(0) === "#") ? h.substring(1,7) : h
  const hexToR = (h) => parseInt((cutHex(h)).substring(0,2),16)
  const hexToG = (h) => parseInt((cutHex(h)).substring(2,4),16)
  const hexToB = (h) => parseInt((cutHex(h)).substring(4,6),16)

  let hRed   = hexToR(hex)
  let hGreen = hexToG(hex)
  let hBlue  = hexToB(hex)

  return {
    hex: hex,
    r: hRed,
    g: hGreen,
    b: hBlue,
  }
}

function luminanace({r, g, b}) {
    var a = [r,g,b].map(function(v) {
        v /= 255;
        return (v <= 0.03928) ?
            v / 12.92 :
            Math.pow( ((v+0.055)/1.055), 2.4 );
        });
    return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722;
}
