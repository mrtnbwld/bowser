import MockTracker from './MockTracker'

if (process.env.NODE_ENV !== 'production') {
  MockTracker.initialize()
}

class CoreConnection {
  constructor(){
    this.wakoopa = window.$Wakoopa
    this.setupCloseEvents()
  }

  getTrackerState(callback){
    this.wakoopa.query('trackerStatus', ({paused, pausedUntil, status}) =>
      callback({paused, pausedUntil, status})
    )
  }

  trackerAvailable(){
    return this.wakoopa !== 'undefined'
  }

  getDeviceToken(callback){
    this.wakoopa.query('getDeviceToken', token => callback(token))
  }

  pauseTracker(duration){
    this.wakoopa.signal('pause', duration)
    return true
  }

  unPauseTracker(dispatch){
    this.wakoopa.signal('unpause');
    return true
  }

  checkTrackerStatus(callback){
    this.wakoopa.query('status', status => callback(status === 'ok'));
  }

  setupCloseEvents() {
    window.addEventListener('blur', () => {
      this.wakoopa.signal('panel:close');
    });
    return true
  }

  setCurrentUser(user){
    this.wakoopa.signal('userDialog:setCurrentUser', user)
    this.wakoopa.signal('panel:close')
  }

  getUserIdentificationButtons(callback){
    // this.wakoopa.query('userDialog:getButtonValues')
    this.wakoopa.query('userDialog:getButtonValues', buttons => callback(buttons))
  }

  closePanel(){
    this.wakoopa.signal('panel:close');
    //because chrome panels have little control from the ext side, the window.close is needed.
    window.close();
    return true
  }
}

export let connection = new CoreConnection();
