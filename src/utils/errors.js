export function errorCode(status){
  if(status === 404){
    return 'notFound'
  }else if(status === 504){
    return 'connectionTimeout'
  }else if(status >= 500){
    return 'serverError'
  }else if(status === 401){
    return 'unauthorized'
  }else if(status === 403){
    return 'forbidden'
  }else{
    return 'connectionError'
  }
}
