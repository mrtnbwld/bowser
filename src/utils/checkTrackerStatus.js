import {connection} from '../utils/CoreConnection'
import { history } from '../store/createStore'

function checkTrackerStatus(nextState, callback) {
  if(connection.trackerAvailable()){
    connection.checkTrackerStatus(ok => {
      if(!ok){
        history.push('/error')
      }
      callback()
    })
  }else{
    history.push('/error')
  }
}

export default checkTrackerStatus
