import clientConfig from '../clientConfig';
import translations from '../translations';

let clientDefault = clientConfig.defaultLocale;
let supportedLanguages = Object.keys(translations);

function hasExactTranslations(lang) {
  return supportedLanguages.includes(lang);
}

function getLanguageFromBaseLanguage(baseLang) {
  // try to match base language with any available language
  return supportedLanguages.find((lang) => {
    let parts = lang.split("-");

    // we only consider xx-XX locales that start with baseLang
    return (parts.length === 2 && parts[0] === baseLang);
  });
}

function rewriteLanguageCode(lang) {
  let parts = lang.split("-");
  let result = [];

  if (parts.length === 1) {
    // de => de and de-DE
    result.push(parts[0]);
    result.push(parts[0] + '-' + parts[0].toUpperCase());

    // special case for en => en-US
    if (parts[0] === 'en') {
      result.push('en-US');
    }
  } else {
    // de-de => de-DE
    // en-us => en-US
    result.push(parts[0] + '-' + parts[1].toUpperCase());
  }

  return result;
}

function getBrowserLanguages() {
  let rewrittenLanguages = [];

  // navigator.languages returns language codes in order of preference
  // codes can be en, de, nl, but also en-US, etc
  //
  // we rewrite all these language codes to be compatible with the locale
  // files we use in the user interface, which always include the region-
  // specific language code (eg: de-DE instead of de)
  //
  // TODO: allow a language code like de to be picked up by a language code
  //       like de-NA, and this is also pending a reform of language codes in
  //       general in client_config
  navigator.languages.forEach(function(lang) {
    rewriteLanguageCode(lang).forEach(function(lang) {
      rewrittenLanguages.push(lang);
    });
  });

  return rewrittenLanguages;
}

export function localePreferences() {
  // try the locales in the browser preferences,
  // eg nl-NL matches nl-NL
  let browserLanguages = getBrowserLanguages();
  for (let lang of browserLanguages) {
    if (hasExactTranslations(lang)) {
      return lang;
    }
  }

  // try any "base" locales that don't fully match,
  // eg: nl matches nl-BE
  for (let baseLang of browserLanguages.filter((l) => l.length === 2)) {
    let lang = getLanguageFromBaseLanguage(baseLang);

    if (lang) {
      return lang;
    }
  }

  // try the client default
  if (hasExactTranslations(clientDefault)) {
    return clientDefault;
  }

  // try to rewrite the client default
  let rewritten = rewriteLanguageCode(clientDefault).filter((l) => l !== clientDefault);
  if (rewritten.length > 0 && hasExactTranslations(rewritten[0])) {
    return rewritten[0];
  }

  // fallback to english
  return 'en-US';
}

export default { localePreferences };
