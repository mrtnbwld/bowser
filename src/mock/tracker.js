import moment from 'moment';
import clientConfig from '../clientConfig'

const tracker = {
  paused: false,
  pausedUntil: null,
  status: 'ok',

  //
  // helper functions
  //
  isPaused: function() {
    if (this.pausedUntil && moment(this.pausedUntil).diff(moment()) < 0) {
      this.paused = false;
    }
    return this.paused;
  },

  getUnpauseAt: function() {
    if (this.paused && !this.pausedUntil) {
      let time = new Date();
      this.pausedUntil = time.setTime(time.getTime() + 1*1000).getTime();
    }
    return this.pausedUntil;
  },

  //
  // query/signal functions
  //
  query: function(query, param, _callback){
    // console.log('query:', query);
    if (typeof param === 'function') {
      _callback = param;
      param = undefined;
    }

    var returnvalue;
    switch(query) {
      case 'status':
        returnvalue = this.status;
        break;
      case 'paused':
        returnvalue = this.isPaused();
        break;
      case 'unpauseAt':
        returnvalue = this.getUnpauseAt();
        break;
      case 'trackerStatus':
        returnvalue = {
          paused: this.isPaused(),
          pausedUntil: this.getUnpauseAt(),
          status: this.status
        };
        break;
      case 'userDialog:getButtonValues':
        if(clientConfig.clientCode === "cmb"){
          returnvalue = {
            type: "image",
            data: [
              {key: 'parent_user', value: 'user1_value'},
              {key: 'child_user', value: 'user2_value'}
            ]
          }
        }else{
          returnvalue = {
            type: "text",
            data: [
              {key: 'main_user', value: 'user1_value'},
              {key: 'other_user', value: 'user2_value'}
            ]}
        }
        break;
      case 'getDeviceToken':
        returnvalue = "1cd91dcaa3ab592d845bea52541be1b5a8b16ca6";
        break;
      case 'current-url':
        returnvalue = window.location.href;
        break;
      case 'js:storage:get':
        returnvalue = window.localStorage.getItem(param);
        break;
      default:
        console.log('query not supported', query);
        break;
    }

    setTimeout(function(){
      _callback(returnvalue)
    }, 100)
  },


  signal: function(signal, param, param2){
    switch(signal) {
      case 'pause':
        this.paused = true;
        // code from tracking plugin
        let time = new Date();
        time.setTime(time.getTime() + param*1000);
        this.pausedUntil = time;
        break;
      case 'unpause':
        this.paused = false;
        this.pausedUntil = undefined;
        break;
      case 'js:storage:set':
        window.localStorage.setItem(param, param2);
        break;
      default:
        // console.log('signal not supported: ', signal);
        break;
    }
  }
};

export default tracker;
