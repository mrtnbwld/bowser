import { createStore, applyMiddleware, compose } from 'redux'
import { hashHistory } from 'react-router'
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux'
import {reducers} from '../reducers'
import thunk from 'redux-thunk'
import apiRequest from '../middleware/apiRequest'
import authHeaders from '../middleware/authHeaders'
import errorHandler from '../middleware/errorHandler'
import trackerHandler from '../middleware/trackerHandler'
import warningHandler from '../middleware/warningHandler'
import submitSupportForm from '../middleware/submitSupportForm'
// Add the reducer to your store on the `routing` key
const reduxRouterMiddleware = routerMiddleware(hashHistory)

let store;
let middleware = applyMiddleware(
  thunk,
  authHeaders,
  apiRequest,
  errorHandler,
  warningHandler,
  reduxRouterMiddleware,
  trackerHandler,
  submitSupportForm
)

if (process.env.NODE_ENV === 'development' && window.devToolsExtension){
  store = createStore(
    reducers,
    undefined,
    compose(
      middleware,
      window.devToolsExtension && window.devToolsExtension()
    )
  )
} else {
  store = createStore(
    reducers,
    undefined,
    compose(middleware)
  )

}


const history = syncHistoryWithStore(hashHistory, store)

export default store;
export { store, history };
