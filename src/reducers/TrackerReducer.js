import {
  TRACKER_STATE,
  PAUSE_TRACKER,
  UNPAUSE_TRACKER,
  APP_START_FINISHED
} from '../actions'

const defaultState = {
  appStarted: false,
  paused: false,
  status: false,
  pausedUntil: undefined
}

function trackerReducer(state = defaultState, action) {
  switch(action.type){
    case APP_START_FINISHED:
      return {
        ...state,
        appStarted: true
      }
    case TRACKER_STATE:
      let trackerState = action.payload.trackerState
      if(trackerState.pausedUntil === null){ trackerState.pausedUntil = undefined}
      return {...state, ...action.payload.trackerState}
    case PAUSE_TRACKER:
      return {...state, ...action.payload.trackerState}
    case UNPAUSE_TRACKER:
      return {...state, ...action.payload.trackerState, pausedUntil: undefined}
    default:
      return state
  }
}

export {trackerReducer}
