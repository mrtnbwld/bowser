import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { devicesReducer } from './DevicesReducer'
import { warningReducer } from './WarningReducer'
import { trackerReducer } from './TrackerReducer'
import { participationReducer } from './ParticipationReducer'
import { i18nReducer } from 'react-redux-i18n';


const reducers = combineReducers({
  routing: routerReducer,
  i18n: i18nReducer,
  devices: devicesReducer,
  warnings: warningReducer,
  tracker: trackerReducer,
  participation: participationReducer
})

export {reducers};
