import {
  FETCHED_DEVICES_LIST_SUCCESS,
  FETCHED_DEVICES_LIST_ERROR
} from '../actions'

const defaultState = []

function devicesReducer(state = defaultState, action) {
  switch(action.type){
    case FETCHED_DEVICES_LIST_SUCCESS:
      return action.payload.devices
    case FETCHED_DEVICES_LIST_ERROR:
      return state
    default:
      return state
  }
}

export {devicesReducer}
