import {
  ADD_WARNING,
  REMOVE_WARNING
} from '../actions'

const defaultState = []

function warningReducer(state = defaultState, action) {
  switch(action.type){
    case ADD_WARNING:
      return [
          {
            name: action.payload.name,
            action: action.payload.action
          },...state
        ]
    case REMOVE_WARNING:
      return state.filter((w) => {
        return w.name !== action.payload.name
      })
    default:
      return state
  }
}

export {warningReducer}
