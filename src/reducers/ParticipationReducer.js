import {
  DEVICE_AUTH_HASH,
  FETCHED_PARTICIPATION_STATUS_START,
  FETCHED_PARTICIPATION_STATUS_END,
  FETCHED_PARTICIPATION_STATUS_SUCCESS,
  FETCHED_PARTICIPATION_STATUS_ERROR
} from '../actions'

const defaultState = {
  deviceAuthHash: undefined,
  deviceRemoved: false,
  loading: false,
  participating: true
}

// NOTE: `active` is not related to activity but if the device/participant is deleted or not
function participationReducer(state = defaultState, action) {
  const payload = action.payload

  switch(action.type){
    case DEVICE_AUTH_HASH:
      return {...state, deviceAuthHash: action.payload.deviceAuthHash}
    case FETCHED_PARTICIPATION_STATUS_START:
      return {...state, loading: true}
    case FETCHED_PARTICIPATION_STATUS_END:
      return {...state, loading: false}
    case FETCHED_PARTICIPATION_STATUS_SUCCESS:
      const device = payload.device
      return {...state,
        deviceRemoved: device.stopped_participation,
        participating: !device.stopped_participation
      }
    case FETCHED_PARTICIPATION_STATUS_ERROR:
      if(payload.status === 404){
        return {...state,
          deviceRemoved: true,
          participating: false
        }
      }
      else{
        return state
      }
    default:
      return state
  }
}

export {participationReducer}
