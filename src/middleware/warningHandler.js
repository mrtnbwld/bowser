import {
  ADD_WARNING,
  REMOVE_WARNING,
  FETCHED_DEVICES_LIST_ERROR,
  FETCHED_DEVICES_LIST_SUCCESS,
  FETCHED_SUPPORT_FORM_ERROR,
  FETCHED_SUPPORT_FORM_SUCCESS
} from '../actions'
// TODO: Refactor warnings
const warningHandler = store => next => action => {
  const {dispatch} = store

  function checkForWarning(name){
    let containsWarning
    store.getState() && store.getState().warnings.each((w) => {
      if(w.name === name) {
        containsWarning = true
      }
    })
    return containsWarning
  }

  if (action.type === FETCHED_DEVICES_LIST_ERROR) {
    dispatch({
      type: ADD_WARNING,
      payload: {
        name: 'couldNotLoadDevices',
        action: true
      }
    })
  }

  if (action.type === FETCHED_DEVICES_LIST_SUCCESS && checkForWarning('couldNotLoadDevices')) {
    dispatch({
      type: REMOVE_WARNING,
      payload: {
        name: 'couldNotLoadDevices'
      }
    })
  }

  if (action.type === FETCHED_SUPPORT_FORM_ERROR) {
    dispatch({
      type: ADD_WARNING,
      payload: {
        name: 'couldNotSendMessage',
        action: true
      }
    })
  }

  if (action.type === FETCHED_SUPPORT_FORM_SUCCESS && checkForWarning('couldNotSendMessage')) {
    dispatch({
      type: REMOVE_WARNING,
      payload: {
        name: 'couldNotSendMessage'
      }
    })
  }

  next(action)
}


export default warningHandler

