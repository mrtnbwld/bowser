import {connection} from '../utils/CoreConnection'
import {
  TRACKER_STATE,
  PAUSE_TRACKER,
  UNPAUSE_TRACKER
} from '../actions'


const actions = [TRACKER_STATE, PAUSE_TRACKER, UNPAUSE_TRACKER]

const trackerHandler = store => next => action => {
  if(actions.includes(action.type)){
    switch(action.type){
      case PAUSE_TRACKER:
        connection.pauseTracker(action.payload.duration)
        break;
      case UNPAUSE_TRACKER:
        connection.unPauseTracker()
        break;
      default:
        break;
    }

    connection.getTrackerState(({paused, pausedUntil, status}) => {
      action.payload = {...action.payload}

      action.payload.trackerState = {
        paused,
        pausedUntil,
        status
      }
      return next(action)
    })
  }else{ return next(action) }
}


export default trackerHandler

