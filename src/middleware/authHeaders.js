// import CryptoJS, {HmacSHA256} from 'crypto-js'
// import {v1 as uuid} from 'uuid'

// const secret = "secret"
// const apiKey = "apiKey"

const authHeaders = store => next => action => {
  return next(action)
  // NOT NEEDED FOR NOW
  // if(action.type && action.type.includes('REQUEST')){
  //   const signedEndpoint = action.payload.endpoint
  //   const nonce          = uuid().replace(/-/g, '')
  //   const timestamp      = Math.floor(Date.now() / 1000)
  //   const message        = timestamp + nonce
  //   const signature      = CryptoJS.enc.Hex.stringify(HmacSHA256(message, secret)) //HMAC256 the message
  //   const params         = action.payload.params ? '&' + action.payload.params.join().replace(/,/g, '&') : ''
  //   action.payload.endpoint = `${signedEndpoint}?api_key=${apiKey}&nonce=${nonce}&timestamp=${timestamp}&signature=${signature}${params}`;
  //   next(action)
  // }else{
  //   return next(action)
  // }
}

export default authHeaders

