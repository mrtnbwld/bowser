import { push } from 'react-router-redux'
import {
  SUBMIT_SUPPORT_FORM,
  REQUEST_SUPPORT_FORM,
  FETCHED_SUPPORT_FORM_SUCCESS
} from '../actions'

const submitSupportForm = store => next => action => {
  next(action)
  if(action.type === SUBMIT_SUPPORT_FORM){
    const act = {
      type: REQUEST_SUPPORT_FORM,
      payload: {
        endpoint: 'api/v1/support_messages',
        method: 'POST',
        body: {
          support_messages: {
            // This will be implemented as soon as the participantID is available
            // participant_id: store.getState().deviceInfo.participantId,
            from: action.payload.email,
            body: action.payload.body
          }
        }
      },
      meta: action.meta
    }
    store.dispatch(act)
  }else if (action.type === FETCHED_SUPPORT_FORM_SUCCESS){
    store.dispatch(push('/message-sent'))
  }
}

export default submitSupportForm

