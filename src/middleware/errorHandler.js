// import { push } from 'react-router-redux'

// This may be used in the future for handling errors that might pop up during the flow
const errorHandler = store => next => action => {
  // const {dispatch} = store

  // if (action.type.includes('ERROR')) {
  //   dispatch(push('/error'))
  // }

  next(action)
}


export default errorHandler

