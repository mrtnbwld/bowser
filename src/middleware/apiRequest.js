import {errorCode} from '../utils/errors'
import clientConfig from '../clientConfig'

// const domain = 'http://localhost:3000/'
const domain = clientConfig.rootDomain

const detectPrefix = 'REQUEST_'
const actionPrefix = 'FETCHED_'

const apiRequest = store => next => action => {
  const {dispatch} = store
  next(action)
  if (action.type.substring(0, detectPrefix.length) === detectPrefix) {
    const resourceName = action.type.slice(detectPrefix.length)
    dispatch({type: `${actionPrefix}${resourceName}_START`})

    fetch(`${domain}${action.payload.endpoint}`, {
        method: action.payload.method,
        body: JSON.stringify(action.payload.body),
        headers: new Headers({
          'Content-Type': 'application/json'
        })
      })
      .then((response) => {
        if(response.ok) {
          return response.json()
        } else {
          throw new Error ({
            status: response.status,
            detail: response.detail,
          })
        }
      }).then((data) => {
        dispatch({
          type: `${actionPrefix}${resourceName}_SUCCESS`,
          payload: data,
          meta: action.meta,
        })
      }).catch(err => {
        dispatch({
          type: `${actionPrefix}${resourceName}_ERROR`,
          payload: {
            errorCode: errorCode(err.status),
            status: err.status,
            detail: err.detail
          },
          meta: action.meta,
          error: true,
        })
        // if(__DEV__){ console.log('ERROR: ',err) }
      }).then(() => {
        dispatch({type: `${actionPrefix}${resourceName}_END`})
      })

  }

}


export default apiRequest

