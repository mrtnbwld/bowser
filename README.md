# User Interface

Run the project seperately using `yarn start` or build it using `yarn build`. Running the project will automatically open it in a window of your browser on `localhost:4200`. Building automatically puts the ouput in `build/panel` for the plugin extensions.

Other commands are listed in the `package.json`

## Using Redux DevTools

After downloading and installing the [Redux DevTools Extension](http://extension.remotedev.io/), you can start using it in the window in which the project is currently running.


### Dispatching actions

To dispatch [Redux](http://redux.js.org/) actions to alter the App state, you can open the Redux DevTools and its dispatcher (a tiny keyboard icon). It is recommended to enlarge the plugin by clicking on one of the bottom left icons within the plugin for usability reasons.

### Actions
To dispatch an action, paste or write the action in the dispatcher and click on `dispatch`.

#### Router location
```javascript
{
  type: '@@router/LOCATION_CHANGE',
  payload: {
    pathname: '/support',
    search: ''
  }
}
````

Available pathnames:
```
/
/privacy
/support
/message-sent
/error
/user-identification
```
#### Triggering the Killswitch
While on the homepage (pathname `/`), use:
```javascript
{
  type: 'FETCHED_PARTICIPATION_STATUS_SUCCESS',
  payload: {
    device: {
      active: false
    }
  }
}
```

