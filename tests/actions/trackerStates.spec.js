import {
  TRACKER_STATE,
  PAUSE_TRACKER,
  UNPAUSE_TRACKER
} from '../../src/actions'
import {getTrackerState, pauseTracker, unpauseTracker} from '../../src/actions/trackerStates'
import isFluxStandardAction from '../testUtils/fluxStandardAction'

describe('getTrackerState action creator: ', () => {
  it('should follow the Flux Standard Action format', ()=> {
    expect(isFluxStandardAction(getTrackerState({}, ''))).toBe(true)
  })

  it('getTrackerState should return a request object', ()=> {
    const expectedAction = {
      type: TRACKER_STATE,
    }
    expect(getTrackerState()).toEqual(expectedAction)
  })
})

describe('pauseTracker action creator: ', () => {
  it('should follow the Flux Standard Action format', ()=> {
    expect(isFluxStandardAction(pauseTracker({}, ''))).toBe(true)
  })

  it('pauseTracker should return a request object', ()=> {
    const expectedAction = {
      type: PAUSE_TRACKER,
      payload: {
        duration: 900
      }
    }
    expect(pauseTracker(900)).toEqual(expectedAction)
  })
})

describe('unpauseTracker action creator: ', () => {
  it('should follow the Flux Standard Action format', ()=> {
    expect(isFluxStandardAction(unpauseTracker({}, ''))).toBe(true)
  })

  it('unpauseTracker should return a request object', ()=> {
    const expectedAction = {
      type: UNPAUSE_TRACKER,
    }
    expect(unpauseTracker()).toEqual(expectedAction)
  })
})
