import {
  SUBMIT_SUPPORT_FORM
} from '../../src/actions'
import {submitForm, formDataToMessage} from '../../src/actions/supportForm'
import isFluxStandardAction from '../testUtils/fluxStandardAction'

describe('submitForm action creator: ', () => {
  it('should follow the Flux Standard Action format', ()=> {
    expect(isFluxStandardAction(submitForm({}, ''))).toBe(true)
  })

  it('submitForm should return a request object', ()=> {
    const data = {
      email: 'test@wakoopa.com',
      subject: 'general',
      message: 'test'
    }
    const email = 'test@wakoopa.com'
    const expectedAction = {
      type: SUBMIT_SUPPORT_FORM,
      payload: {
        body: 'email: test@wakoopa.com\nsubject: general\nmessage: test\n',
        email: 'test@wakoopa.com'
      }
    }
    expect(submitForm(data, email)).toEqual(expectedAction)
  })
})

describe('formDataToMessage action creator: ', () => {
  it('formDataToMessage should return a request object', ()=> {
    const data = {
      email: 'test@wakoopa.com',
      subject: 'general',
      message: 'test'
    }
    const expectedAction = 'email: test@wakoopa.com\nsubject: general\nmessage: test\n'
    expect(formDataToMessage(data)).toEqual(expectedAction)
  })
})
