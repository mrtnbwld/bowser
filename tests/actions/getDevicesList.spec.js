import {
  REQUEST_DEVICES_LIST
} from '../../src/actions'
import {getDevicesList} from '../../src/actions/getDevicesList'
import isFluxStandardAction from '../testUtils/fluxStandardAction'

describe('getDevicesList action creator: ', () => {
  it('should follow the Flux Standard Action format', ()=> {
    expect(isFluxStandardAction(getDevicesList())).toBe(true)
  })

  it('getDevicesList should return a request object', ()=> {
    const expectedAction = {
      type: REQUEST_DEVICES_LIST,
      payload: {
        endpoint: 'api/v1/participants/123/devices',
        method: 'GET'
      }
    }
    expect(isFluxStandardAction(getDevicesList(123))).toBe(true)
  })
})
