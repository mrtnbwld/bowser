import {
  PAUSE_TRACKER,
  TRACKER_STATE,
  UNPAUSE_TRACKER
} from '../../src/actions'
import trackerHandler from '../../src/middleware/trackerHandler'

describe('trackerHandler middleware', () => {
  let next, dispatch, getState, middleware, dispatchCalls, nextCalls

  beforeEach(() => {
    next            = jest.fn()
    dispatch        = jest.fn()
    getState        = jest.fn()
    middleware      = trackerHandler({ dispatch, getState })(next)
    dispatchCalls   = dispatch.mock.calls
    nextCalls       = next.mock.calls
  })

  // TODO: find out how to mock CoreConnection
  it('should process TRACKER_STATE', () => {
    middleware({type: TRACKER_STATE})
    expect(dispatchCalls.length).toBe(0)
  })

  it('should process PAUSE_TRACKER', () => {
    middleware({type: PAUSE_TRACKER, payload: {}})
    expect(dispatchCalls.length).toBe(0)
  })

  it('should process UNPAUSE_TRACKER', () => {
    middleware({type: UNPAUSE_TRACKER})
    expect(dispatchCalls.length).toBe(0)
  })

})
