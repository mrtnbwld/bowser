import authHeaders from '../../src/middleware/authHeaders'
describe('authHeaders middleware', () => {
  let next, dispatch, getState, middleware, dispatchCalls, nextCalls

  beforeEach(() => {
    next            = jest.fn()
    dispatch        = jest.fn()
    getState        = jest.fn()
    middleware      = authHeaders({ dispatch, getState })(next)
    dispatchCalls   = dispatch.mock.calls
    nextCalls       = next.mock.calls
  })

  it('should not change the request object now', () => {
    middleware({
      type: 'REQUEST',
      payload: {
        endpoint: 'test'
      }
    })
    expect(dispatchCalls.length).toBe(0)
    expect(nextCalls.length).toBe(1)
    expect(nextCalls).toEqual([[{
      type: 'REQUEST',
      payload: {
        endpoint: 'test'
      }
    }]])
  })

  // beforeEach(() => {
  //   next            = jest.fn()
  //   dispatch        = jest.fn()
  //   getState        = jest.fn()
  //   middleware      = authHeaders({ dispatch, getState })(next)
  //   dispatchCalls   = dispatch.mock.calls
  //   nextCalls       = next.mock.calls
  // })

  // it('should process any REQUEST', () => {
  //   middleware({
  //     type: 'REQUEST',
  //     payload: {
  //       endpoint: 'test'
  //     }
  //   })
  //   expect(dispatchCalls.length).toBe(0)
  //   expect(nextCalls.length).toBe(1)
  //   expect(nextCalls).not.toBe([[{
  //     type: 'REQUEST',
  //     payload: {
  //       endpoint: 'test'
  //     }
  //   }]])
  // })

  // it('should process any REQUEST with a single param', () => {
  //   middleware({
  //     type: 'REQUEST',
  //     payload: {
  //       endpoint: 'test',
  //       params: ['one']
  //     }
  //   })
  //   let endpoint = nextCalls[0][0].payload.endpoint
  //   expect(dispatchCalls.length).toBe(0)
  //   expect(nextCalls.length).toBe(1)
  //   expect(endpoint.substr(endpoint.length - 4)).toEqual('&one')
  // })

  // it('should process any REQUEST with multiple params', () => {
  //   let endpoint = "test"
  //   middleware({
  //     type: 'REQUEST',
  //     payload: {
  //       endpoint: endpoint,
  //       params: ['one', 'two', 'three']
  //     }
  //   })
  //   // let endpoint = nextCalls[0][0].payload.endpoint
  //   expect(dispatchCalls.length).toBe(0)
  //   expect(nextCalls.length).toBe(1)
  //   expect(endpoint).toEqual(endpoint + '&one&two&three')
  // })
})
