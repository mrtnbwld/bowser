import {
  SUBMIT_SUPPORT_FORM,
  REQUEST_SUPPORT_FORM,
  FETCHED_SUPPORT_FORM_SUCCESS
} from '../../src/actions'
import submitSupportForm from '../../src/middleware/submitSupportForm'
describe('submitSupportForm middleware', () => {
  let next, dispatch, getState, middleware, dispatchCalls, nextCalls

  beforeEach(() => {
    next            = jest.fn()
    dispatch        = jest.fn()
    getState        = jest.fn()
    middleware      = submitSupportForm({ dispatch, getState })(next)
    dispatchCalls   = dispatch.mock.calls
    nextCalls       = next.mock.calls
  })

  it('should process SUBMIT_SUPPORT_FORM', () => {
    middleware({
      type: SUBMIT_SUPPORT_FORM,
      payload: {
        email: 'test@test.com',
        body: 'test'
      },
      meta: undefined
    })
    expect(dispatchCalls.length).toBe(1)
    expect(dispatchCalls[0]).toEqual([{
      type: REQUEST_SUPPORT_FORM,
      payload: {
        endpoint: 'api/v1/support_messages',
        method: 'POST',
        body: {
          support_messages: {
            from: 'test@test.com',
            body: 'test'
          }
        }
      },
      meta: undefined
    }])
  })
  it('should process FETCHED_SUPPORT_FORM_SUCCESS', () => {
    middleware({
      type: FETCHED_SUPPORT_FORM_SUCCESS
    })
    expect(dispatchCalls.length).toBe(1)
  })
})
