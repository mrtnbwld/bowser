import apiRequest from '../../src/middleware/apiRequest'
describe('apiRequest middleware', () => {
  let next, dispatch, getState, middleware, dispatchCalls, nextCalls

  beforeEach(() => {
    next            = jest.fn()
    dispatch        = jest.fn()
    getState        = jest.fn()
    middleware      = apiRequest({ dispatch, getState })(next)
    dispatchCalls   = dispatch.mock.calls
    nextCalls       = next.mock.calls
  })

  it('should process any REQUEST', () => {
    middleware({
      type: 'REQUEST_TEST',
      payload: {
        test: 'test'
      }
    })
    expect(dispatchCalls.length).toBe(1)
    expect(nextCalls.length).toBe(1)
    expect(nextCalls).toEqual([[{
      type: 'REQUEST_TEST',
      payload: {
        test: 'test'
      }
    }]])
  })
})
