export default function isFluxStandardAction(action){
  return isObject(action) &&
    hasOnlyAllowedKeys(action) &&
    hasTypeKey(action) &&
    typeKeyIsString(action)
}

function isObject(subject){
  return subject !== null && typeof subject === 'object'
}

function hasOnlyAllowedKeys(action){
  const allowedKeys = ['type', 'payload', 'error', 'meta']
  const keys = Object.keys(action)

  return keys.filter(k => !allowedKeys.includes(k)).length === 0 ? true : false
}

function hasTypeKey(action){
  return Object.keys(action).includes("type")
}

function typeKeyIsString(action){
  return typeof action.type  === 'string'
}
