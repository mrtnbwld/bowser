import React from 'react';
import Support from '../../src/components/Support'
import {Provider} from 'react-redux'
import store from '../../src/store/createStore'
import renderer from 'react-test-renderer';

it('renders Support correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <Support/>
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
