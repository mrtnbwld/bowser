import React from 'react';
import MessageSent from '../../src/components/MessageSent'
import {Provider} from 'react-redux'
import store from '../../src/store/createStore'
import renderer from 'react-test-renderer';

it('renders MessageSent correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <MessageSent/>
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
