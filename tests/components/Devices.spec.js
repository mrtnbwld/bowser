import React from 'react';
import Devices from '../../src/components/Devices'
import {Provider} from 'react-redux'
import store from '../../src/store/createStore'
import renderer from 'react-test-renderer';

it('renders Devices correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <Devices
        devices={[
          {
            install_started_at: undefined,
            first_activity_at: undefined,
            kind: 'tablet'
          }
        ]}
        getDevicesList={()=>{}}
      />
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders Devices with warning correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <Devices
        warnings={[
          {
            name: 'couldNotSendMessage',
            action: false
          }
        ]}
        getDevicesList={()=>{}}
      />
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
