import React from 'react';
import Home from '../../src/components/Home'
import {Provider} from 'react-redux'
import store from '../../src/store/createStore'
import renderer from 'react-test-renderer';

it('renders Home correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <Home
        participation={{}}
        getParticipationStatus={() => {}}
        tracker={{}}
        errors={[]}
      />
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
