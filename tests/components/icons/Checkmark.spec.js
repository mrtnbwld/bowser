import React from 'react';
import Checkmark from '../../../src/components/icons/Checkmark'
import renderer from 'react-test-renderer';

it('renders Checkmark correctly', () => {
  const tree = renderer.create(
    <Checkmark/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
