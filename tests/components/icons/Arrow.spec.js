import React from 'react';
import Arrow from '../../../src/components/icons/Arrow'
import renderer from 'react-test-renderer';

it('renders an active Arrow correctly', () => {
  const tree = renderer.create(
    <Arrow active/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders an inactive Arrow correctly', () => {
  const tree = renderer.create(
    <Arrow/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
