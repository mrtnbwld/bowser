import React from 'react';
import Confirm from '../../../src/components/icons/Confirm'
import renderer from 'react-test-renderer';

it('renders Confirm correctly', () => {
  const tree = renderer.create(
    <Confirm/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
