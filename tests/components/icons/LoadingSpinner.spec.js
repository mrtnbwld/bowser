import React from 'react';
import LoadingSpinner from '../../../src/components/icons/LoadingSpinner'
import renderer from 'react-test-renderer';

it('renders LoadingSpinner correctly', () => {
  const tree = renderer.create(
    <LoadingSpinner/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
