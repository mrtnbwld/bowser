import React from 'react';
import LongArrow from '../../../src/components/icons/LongArrow'
import renderer from 'react-test-renderer';

it('renders LongArrow correctly', () => {
  const tree = renderer.create(
    <LongArrow/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
