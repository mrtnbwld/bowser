import React from 'react';
import Smartphone from '../../../src/components/icons/Smartphone'
import renderer from 'react-test-renderer';

it('renders Smartphone correctly', () => {
  const tree = renderer.create(
    <Smartphone/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders Smartphone in clientColor correctly', () => {
  const tree = renderer.create(
    <Smartphone clientColor/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders an inactive Smartphone correctly', () => {
  const tree = renderer.create(
    <Smartphone status='inactive'/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders a paused Smartphone correctly', () => {
  const tree = renderer.create(
    <Smartphone status='paused'/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
