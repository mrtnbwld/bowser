import React from 'react';
import Bell from '../../../src/components/icons/Bell'
import renderer from 'react-test-renderer';

it('renders Bell correctly', () => {
  const tree = renderer.create(
    <Bell/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
