import React from 'react';
import Desktop from '../../../src/components/icons/Desktop'
import renderer from 'react-test-renderer';

it('renders Desktop correctly', () => {
  const tree = renderer.create(
    <Desktop/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders Desktop in clientColor correctly', () => {
  const tree = renderer.create(
    <Desktop/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders an inactive Desktop correctly', () => {
  const tree = renderer.create(
    <Desktop status='inactive'/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders a paused Desktop correctly', () => {
  const tree = renderer.create(
    <Desktop status='paused'/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
