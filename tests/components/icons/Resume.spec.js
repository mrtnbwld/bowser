import React from 'react';
import Resume from '../../../src/components/icons/Resume'
import renderer from 'react-test-renderer';

it('renders Resume correctly', () => {
  const tree = renderer.create(
    <Resume/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
