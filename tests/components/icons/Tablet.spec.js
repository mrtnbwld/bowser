import React from 'react';
import Tablet from '../../../src/components/icons/Tablet'
import renderer from 'react-test-renderer';

it('renders Tablet correctly', () => {
  const tree = renderer.create(
    <Tablet/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders Tablet in clientColor correctly', () => {
  const tree = renderer.create(
    <Tablet/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders an inactive Tablet correctly', () => {
  const tree = renderer.create(
    <Tablet status='inactive'/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders a paused Tablet correctly', () => {
  const tree = renderer.create(
    <Tablet status='paused'/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
