import React from 'react';
import Stop from '../../../src/components/icons/Stop'
import renderer from 'react-test-renderer';

it('renders Stop correctly', () => {
  const tree = renderer.create(
    <Stop/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
