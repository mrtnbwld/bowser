import React from 'react';
import Add from '../../../src/components/icons/Add'
import renderer from 'react-test-renderer';

it('renders Add correctly', () => {
  const tree = renderer.create(
    <Add/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
