import React from 'react';
import Pause from '../../../src/components/icons/Pause'
import renderer from 'react-test-renderer';

it('renders Pause correctly', () => {
  const tree = renderer.create(
    <Pause/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
