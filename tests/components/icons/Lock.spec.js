import React from 'react';
import Lock from '../../../src/components/icons/Lock'
import renderer from 'react-test-renderer';

it('renders Lock correctly', () => {
  const tree = renderer.create(
    <Lock/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
