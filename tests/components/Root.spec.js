import React from 'react';
import Root from '../../src/components/Root'

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders Root correctly', () => {
  const tree = renderer.create(
    <Root/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
