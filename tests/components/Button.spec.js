import React from 'react';
import Button from '../../src/components/Button'

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders Button correctly', () => {
  const tree = renderer.create(
    <Button/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders Button with an action', () => {
  const tree = renderer.create(
    <Button action={()=>{}}/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders Button with a label', () => {
  const tree = renderer.create(
    <Button label='test label'/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders Button with a loadingSpinner', () => {
  const tree = renderer.create(
    <Button inProgress={true} progressLabel='test progressLabel'/>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

