import React from 'react';
import Privacy from '../../src/components/Privacy'
import {Provider} from 'react-redux'
import store from '../../src/store/createStore'
import renderer from 'react-test-renderer';

it('renders Privacy correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <Privacy/>
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
