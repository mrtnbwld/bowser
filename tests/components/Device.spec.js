import React from 'react';
import Device from '../../src/components/Devices/Device'
import {Provider} from 'react-redux'
import store from '../../src/store/createStore'
import renderer from 'react-test-renderer';

it('renders a Tablet Device correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <Device device={
        {
          install_started_at: undefined,
          first_activity_at: undefined,
          kind: 'tablet'
        }
      }/>
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})


it('renders a Smartphone Device correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <Device device={
        {
          install_started_at: undefined,
          first_activity_at: undefined,
          kind: 'smartphone'
        }
      }/>
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders a Desktop Device correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <Device device={
        {
          install_started_at: undefined,
          first_activity_at: undefined,
          kind: 'desktop'
        }
      }/>
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders a default Device correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <Device device={{}}/>
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

