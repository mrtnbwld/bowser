import React from 'react';
import StoppedDevices from '../../src/components/Devices/StoppedDevices'
import Device from '../../src/components/Devices/Device'
import {Provider} from 'react-redux'
import store from '../../src/store/createStore'
import renderer from 'react-test-renderer';

it('renders StoppedDevices correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <StoppedDevices devices={[
        {
          install_started_at: undefined,
          first_activity_at: undefined,
          kind: 'tablet'
        }
        ]}>
        <Device/>
      </StoppedDevices>
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
