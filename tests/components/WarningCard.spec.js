import React from 'react';
import WarningCard from '../../src/components/WarningCard'
import {Provider} from 'react-redux'
import store from '../../src/store/createStore'
import renderer from 'react-test-renderer';

it('renders WarningCard correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <WarningCard
        error={{}}
      />
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders WarningCard with an action correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <WarningCard
        error={{}}
        action={() => {}}
      />
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders WarningCard with instructions correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <WarningCard
        error={{
          instructions: true
        }}
      />
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
