import React from 'react';
import DisplayError from '../../src/components/DisplayError'
import WarningCard from '../../src/components/WarningCard'
import {Provider} from 'react-redux'
import store from '../../src/store/createStore'
import renderer from 'react-test-renderer';

it('renders DisplayError correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <DisplayError errors={[{name: 'notConfigured'}]} location={{}}/>
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
