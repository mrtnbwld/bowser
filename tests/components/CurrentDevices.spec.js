import React from 'react';
import CurrentDevices from '../../src/components/Devices/CurrentDevices'
import {Provider} from 'react-redux'
import store from '../../src/store/createStore'
import renderer from 'react-test-renderer';

it('renders CurrentDevices correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <CurrentDevices/>
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
