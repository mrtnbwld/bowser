import React from 'react';
import App from '../../src/components/App'
import {Provider} from 'react-redux'
import store from '../../src/store/createStore'
import renderer from 'react-test-renderer';

it('renders App correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <App
        location={{}}
        getTrackerState={()=>{}}
        getParticipationStatus={()=>{}}
      />
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
