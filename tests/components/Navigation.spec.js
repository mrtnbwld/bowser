import React from 'react';
import Navigation from '../../src/components/Navigation'
import {Provider} from 'react-redux'
import store from '../../src/store/createStore'
import renderer from 'react-test-renderer';

it('renders Navigation for "support" and "message sent" correctly', () => {
  const tree = renderer.create(
      <Navigation
        location={{pathname: '/support'}}
      />
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders Navigation for "my-devices" correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <Navigation
        location={{pathname: '/my-devices'}}
      />
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})

it('renders Navigation for "privacy" correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <Navigation
        location={{pathname: '/privacy'}}
      />
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot()
})
