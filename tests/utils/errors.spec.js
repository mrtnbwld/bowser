import {errorCode} from '../../src/utils/errors'

describe('errorCode util', () => {

  it('should return status 404', () => {
    expect(errorCode(404)).toEqual('notFound')
  })
  it('should return status 504', () => {
    expect(errorCode(504)).toEqual('connectionTimeout')
  })
  it('should return status 500', () => {
    expect(errorCode(500)).toEqual('serverError')
  })
  it('should return status 401', () => {
    expect(errorCode(401)).toEqual('unauthorized')
  })
  it('should return status 403', () => {
    expect(errorCode(403)).toEqual('forbidden')
  })
  it('should return a default error', () => {
    expect(errorCode()).toEqual('connectionError')
  })

})
