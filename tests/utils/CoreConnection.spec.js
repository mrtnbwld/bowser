import {connection} from '../../src/utils/CoreConnection'

describe('CoreConnection util', () => {

  //TODO: find out how to mock wakoopa object
  it('should getTrackerState', () => {
    connection.getTrackerState( (state) => {
      expect(state).toBe(typeof Object)
      expect(Object.keys(state).length).toBe(3)
    })
  })
  it('should tell if the tracker is available', () => {
    expect(connection.trackerAvailable()).toBe(true)
  })
  it('should signal the tracker to pause', () => {
    expect(connection.pauseTracker()).toBe(true)
  })
  it('should signal the tracker to unpause', () => {
    expect(connection.unPauseTracker()).toBe(true)
  })
  it('should check if the tracker status is ok', () => {
    connection.checkTrackerStatus(status => {
      expect(status).toBe(true)
    })
  })
  it('should setupCloseEvents', () => {
    expect(connection.setupCloseEvents()).toBe(true)
  })
  it('should closePanel', () => {
    expect(connection.closePanel()).toBe(true)
  })
})
