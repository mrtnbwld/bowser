import {
  PAUSE_TRACKER,
  UNPAUSE_TRACKER,
  TRACKER_STATE
} from '../../src/actions'
import {pauseTracker, unpauseTracker, getTrackerState} from '../../src/actions/trackerStates'
import {trackerReducer} from '../../src/reducers/TrackerReducer'
import {connection} from '../../src/utils/CoreConnection'

describe('Tracker reducer', () => {
  it('should return the initial tracker state', ()=> {
    expect(trackerReducer(undefined, {})).toEqual({
      paused: false,
      appStarted: false,
      status: false,
      pausedUntil: undefined
    })
  })

  it('should unpause the tracker state', ()=> {
    const action = {
      type: UNPAUSE_TRACKER,
      payload: {
        trackerState: {
          paused: false,
          pausedUntil: undefined,
          status: true
        }
      }
    }
    expect(trackerReducer(undefined, action)).toEqual({
      paused: false,
      appStarted: false,
      pausedUntil: undefined,
      status: true
    })
  })

  it('should pause the tracker state', ()=> {
    let time = new Date()
    time.setTime(time.getTime() + 900*1000);

    let action = {
      type: PAUSE_TRACKER,
      payload: {
        trackerState: {
          paused: true,
          pausedUntil: time,
          status: true
        }
      }
    }

    expect(trackerReducer(undefined, action)).toEqual({
      paused: true,
      appStarted: false,
      pausedUntil: time,
      status: true
    })
  })
})


