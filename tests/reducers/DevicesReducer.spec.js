import {
  ADD_DEVICE,
  FETCHED_DEVICES_LIST_SUCCESS,
  FETCHED_DEVICES_LIST_ERROR
} from '../../src/actions'
import {devicesReducer} from '../../src/reducers/DevicesReducer'

describe('Devices reducer', () => {
  it('it should return the initial state', ()=> {
    expect(devicesReducer(undefined, {})).toEqual([])
  })
})

describe('Devices reducer', () => {
  let action = {
    type: FETCHED_DEVICES_LIST_ERROR
  }
  it('it should return an empty array when an error occurs', ()=> {
    expect(devicesReducer(undefined, action)).toEqual([])
  })
})

describe('Devices reducer', () => {
  let action = {
    type: FETCHED_DEVICES_LIST_SUCCESS,
    payload: {
      devices: [{
        kind: 'tablet'
      }]
    }
  }
  it('it should return an array when successfully fetched', ()=> {
    expect(devicesReducer(undefined, action)).toEqual([
        {
          kind: 'tablet'
        }
      ])
  })
})
